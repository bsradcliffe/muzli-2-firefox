(function(i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r;
    i[r] = i[r] || function() {
        (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date();
    a = s.createElement(o),
        m = s.getElementsByTagName(o)[0];
    a.async = 1;
    a.src = g;
    m.parentNode.insertBefore(a, m)
})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
(function() {
    ga('create', window.GA_TRACKING_CODE, 'auto');
    ga('set', 'checkProtocolTask', function() {}); // Removes failing protocol check. @see: http://stackoverflow.com/a/22152353/1958200
    ga('require', 'displayfeatures');
})();

//install / uninstall
(function() {

    var requestSize = 15;
    var newItems = (Math.floor(Math.random() * (requestSize - 5)) + 5).toString();
    var setUninstallURLDone = false;
    var cacheTime = 60;
    var activeRefreshTime = 15;
    var cachedImages = [];

    //Event listener to track scroll distance events
    window.chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {

        if (request.scrollDistance || request.scrollDistance === 0) {
            ga('send', 'event', 'Scroll distance', request.state, request.source, request.scrollDistance);
        }

        if (request.getUserCookieData) {
            fetchUserCookies(function(cookieData) {
               sendResponse(cookieData);
            })

            return true;
        }
    });

    //uninstall
    function setUninstallURL() {

        if (setUninstallURLDone) {
            return;
        }

        window.chrome.storage.local.get("installDate", function(obj) {
            if (obj.installDate) {
                setUninstallURLDone = true;
                var date = new Date();
                var udate = ("0" + (date.getMonth() + 1)).slice(-2).toString() + ("0" + (date.getDate())).slice(-2).toString() + date.getFullYear().toString();
                window.chrome.runtime.setUninstallURL(window.MUZLI_WEBSITE_URL + "/stay/?idate=" + obj.installDate + "&udate=" + udate, function() {})
            }
        });
    }

    function cacheFeed() {
        
        var oReq = new XMLHttpRequest();
        oReq.onload = reqListener;

        function reqListener() {

            var data = JSON.parse(this.responseText);
            window.chrome.storage.local.set({ 'cachedFeed': data }, function() {});

            //Cache up to 10 images to display in home view
            cachedImages = [];
            
            for (i = 0, length = Math.min(data.feed.length, 10); i < length; ++i) {
                cachedImages[i] = new Image();
                cachedImages[i].src = data.feed[i].image;
            }
        }

        var limit = window.muzli ? window.muzli.paging.server : 60;

        oReq.open('get', window.MUZLI_SERVER + '/cached/muzli?background=true&limit=' + limit, true);
        oReq.send();

        //set badge
        window.chrome.browserAction.setBadgeBackgroundColor({ "color": [255, 52, 102, 255] });
        window.chrome.browserAction.getBadgeText({}, function(r) {
            if (r > 0) {
                r = parseInt(r);
                r = r + 1;
                window.chrome.browserAction.setBadgeText({ "text": r.toString() });
            } else {
                window.chrome.browserAction.setBadgeText({ "text": newItems });
            }
        })
    }

    //Clear user cookie
    function removeUserCookie(key) {
        var a = document.createElement('script');
        a.sync = 0;
        a.src = window.MUZLI_WEBSITE_URL + '/partners/remove.php?key=' + key;
        document.body.appendChild(a);
        document.body.removeChild(a);
    }


    //Load cookies from muz.li website to local storage
    function fetchUserCookies(callback) {

        //The server code executes callback function when loaded
        window.JSON_CALLBACK = function(res) {

            if (!res) {

                if (callback) {
                    callback({});
                }

                return;
            }

            if (res.isLite) {
                window.localStorage.setItem('lite', true);
                window.localStorage.setItem('halfView', true);
            }

            if (res.disableGallery) {
                window.localStorage.setItem('enableSharebleLinks', false);
                removeUserCookie('disableGallery');
            }

            window.chrome.storage.sync.set({ 
                'userCookieData': res, 
                'installTime': new Date().getTime() 
            });

            try {
                window.localStorage.userCookieData = JSON.stringify(res);
            } catch (err) {

                console.error(err);

                if (callback) {
                    callback({});
                }
            }

            if (callback) {
                callback(res);
            }

        };

        var a = document.createElement('script');
        a.sync = 0;
        a.src = window.MUZLI_WEBSITE_URL + '/partners/partner.php';
        document.body.appendChild(a);
    }

    function updateActiveTime() {

        var script = document.createElement('script');
        script.sync = 0;
        script.src = window.MUZLI_WEBSITE_URL + '/partners/checkin.php';

        document.body.appendChild(script);
        document.body.removeChild(script);
    }

    window.chrome.browserAction.onClicked.addListener(function() {
        window.chrome.tabs.create({ 'url': window.chrome.extension.getURL('index.html?button') }, function(tab) {});
    });

    //install reason
    window.chrome.runtime.onInstalled.addListener(function(obj) {

        ga('send', 'event', 'Install', 'load', obj.reason);

        //Set user GDPR settings
        window.chrome.storage.local.get('isGDPRuser', function(storage) {

            var oReq = new XMLHttpRequest();

            oReq.onload = function() {
                var data = JSON.parse(this.responseText);
                window.chrome.storage.local.set({ 'isGDPRuser': !!data.isGDPR }, function() {});
            };

            oReq.open('get', 'https://geolocation-api.invisionapp.com/api/obter', true);
            oReq.send();
        
        })

        if (obj.reason === 'install') {
            
            var date = new Date();
            window.chrome.storage.local.set({ 'installDate': ("0" + (date.getMonth() + 1)).slice(-2).toString() + ("0" + (date.getDate())).slice(-2).toString() + date.getFullYear().toString() });
            
            setUninstallURL();
            fetchUserCookies(function(res) {
                if (!res.isLite) {
                    chrome.tabs.create({url: "chrome://newtab"}, function (tab) {});
                }
            });

        }

        if (obj.reason === 'update') {
            var date = new Date();
            window.chrome.storage.local.set({ 'updateDate': ("0" + (date.getMonth() + 1)).slice(-2).toString() + ("0" + (date.getDate())).slice(-2).toString() + date.getFullYear().toString() });
        }
    });

    //Cache Muli feed every 15 mins 
    window.chrome.alarms.create("cacheFeed", { periodInMinutes: cacheTime });
    window.chrome.alarms.create("updateActiveTime", { periodInMinutes: activeRefreshTime });

    window.chrome.alarms.onAlarm.addListener(function(alarm) {
        
        if (alarm.name === "cacheFeed") {
            cacheFeed();
        }

        if (alarm.name === "updateActiveTime") {
            updateActiveTime();
        }

    });

    window.chrome.storage.local.get('openAfterInit', function(storage) {

        if (storage.openAfterInit) {
            window.chrome.tabs.create({ 'url': window.chrome.extension.getURL('index.html') }, function(tab) {});
        };

        window.chrome.storage.local.set({openAfterInit: false});
    })

    //Feed recache on server change
    window.chrome.storage.onChanged.addListener(function(changes, namespace) {
        if (changes.apiVersion) {

            var apiVersion = changes.apiVersion.newValue || 'v1';

            //Set Different API endpont if user is in V2 bucket
            if (apiVersion === 'v1') {
                window.MUZLI_SERVER = window.MUZLI_SERVER_V1 || window.MUZLI_SERVER;
            } 

            if (apiVersion === 'v2') {
                window.MUZLI_SERVER_V1 = window.MUZLI_SERVER;
                window.MUZLI_SERVER = window.MUZLI_SERVER_V2 || window.MUZLI_SERVER_V1;
            }

            cacheFeed();

        }
    });

    //Initial feed cache
    window.chrome.storage.local.get(['apiVersion'], function(result) {
      
        if (result.apiVersion) {

            var apiVersion = result.apiVersion || 'v1';

            if (apiVersion === 'v2') {
                window.MUZLI_SERVER_V1 = window.MUZLI_SERVER;
                window.MUZLI_SERVER = window.MUZLI_SERVER_V2 || window.MUZLI_SERVER_V1;
            } 
        }

        cacheFeed();

    });


    updateActiveTime();
    setUninstallURL();

})();

