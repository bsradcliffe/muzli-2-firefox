(function () {

	window.MUZLI_SERVER = 'https://alpha.api.muz.li/v1';
	window.MUZLI_SERVER_V2 = 'https://beta.api.muz.li/v1';

	window.MUZLI_ADMIN_SERVER = 'https://admin.muz.li';

	window.MUZLI_AD_SERVER = 'https://bobo.muz.li/ads';
	window.MUZLI_SHARE_SERVER = 'http://s.muz.li/';
	window.MUZLI_CONTENT_SERVER = 'https://content.muz.li';
	window.MUZLI_COLORS_SERVER = 'https://colors.muz.li';
	window.MUZLI_WEBSITE_URL = 'https://muz.li';
	window.MUZLI_SEARCH_URL = 'https://search.muz.li';
	window.MUZLI_COMMUNITY_PUBLIC_URL = 'https://muz.li/community';
	
	window.GA_TRACKING_CODE = 'UA-53926383-11';

})();