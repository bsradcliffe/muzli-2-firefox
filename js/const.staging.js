(function () {

	window.MUZLI_SERVER    = 'https://muzli-api-staging.herokuapp.com/v1';
	window.MUZLI_SERVER_V2 = 'https://muzli-api-es.herokuapp.com/v1';

	window.MUZLI_ADMIN_SERVER = 'https://muzli-admin-staging.herokuapp.com';
	
	window.MUZLI_AD_SERVER = 'https://muzli-admin-es.herokuapp.com/ads';
	window.MUZLI_SHARE_SERVER = 'http://muzli-share-es.herokuapp.com/';
	window.MUZLI_CONTENT_SERVER = 'https://muzli-content-staging.herokuapp.com';
	window.MUZLI_WEBSITE_URL = 'https://muzli-website-qa.herokuapp.com';
	window.MUZLI_COMMUNITY_PUBLIC_URL = 'https://muzli-website-qa.herokuapp.com/community';

	window.GA_TRACKING_CODE = 'UA-53926383-11';

})();