angular.module('muzli-template', [])
  .run(['$templateCache', function($templateCache) {
    $templateCache.put('modules/feed/feed.drv.colors.html',
    '<section id="feed" class="palettes">\n' +
    '\n' +
    '<ul\n' +
    '	infinite-scroll=\'::loadMore()\'\n' +
    '	infinite-scroll-distance=\'::infiniteScrollDistance\'\n' +
    '	infinite-scroll-immediate-check=\'false\'\n' +
    '	infinite-scroll-disabled=\'!feed.length\'>\n' +
    '\n' +
    '	<li feed-item ng-repeat="item in feed track by (item.id || item.link)"\n' +
    '	ng-class="::{\n' +
    '		article: item.source.article && !item.video,\n' +
    '		visited: item.visited,\n' +
    '		hasStats: item.hasStats, pick: item.pick,\n' +
    '		feedSuggest: !!item.promotion,\n' +
    '		fallbackImage: item.fallbackImage,\n' +
    '		showSharePromo: item.displaySharePromo,\n' +
    '		outside: item.isOutside,\n' +
    '		showMenu: item.showMenu,\n' +
    '		}"\n' +
    '	class="angular-animate"\n' +
    '	data-muzli-id="{{::item.id}}">\n' +
    '\n' +
    '		<div class="tile">\n' +
    '\n' +
    '			<div class="feedLink">\n' +
    '\n' +
    '				<span class="icon-image" ng-click="item.showPhoto = !item.showPhoto"></span>\n' +
    '\n' +
    '				<div class="postPhoto">\n' +
    '					<div class="palette" style="background: {{item.palette[item.palette.length - 1]}}" ng-class="{hidden: item.showPhoto}">\n' +
    '						<span ng-repeat="color in item.palette"\n' +
    '							style="background: {{color}}"\n' +
    '							title-top="true"\n' +
    '							click-copy-color="{{::color}}"\n' +
    '							brightness="{{::color}}"\n' +
    '							prevent-click>\n' +
    '							{{color}}\n' +
    '						</span>\n' +
    '					</div>\n' +
    '					<a class="wrapper" ng-mousedown="::postClick(item, $event)" target="_blank" ng-href="{{::item.link_out}}">\n' +
    '						<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAMAAAAoyzS7AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAAZQTFRFAAAAAAAApWe5zwAAAAF0Uk5TAEDm2GYAAAAMSURBVHjaYmAACDAAAAIAAU9tWeEAAAAASUVORK5CYII=" muzli-lazy="{{::item.image}}" muzli-gif="{{::item.gif}}" muzli-is-gif="{{::item.isGif}}" src="" alt="" />\n' +
    '					</a>\n' +
    '				</div>\n' +
    '\n' +
    '				<div class="postInfo">\n' +
    '					<div class="download" title="Download palette (SVG)" ng-click="downloadSVG(item, $event)">Download</div>\n' +
    '				</div>\n' +
    '\n' +
    '			</div>\n' +
    '\n' +
    '			<div class="postMeta">\n' +
    '\n' +
    '				<div class="details angular-animate">\n' +
    '\n' +
    '					<span class="favorite icon-fav" ng-click="::toggleFavorite($event, item)" ng-if="::showFavorite" ng-class="{ active: item.favorite }" title="Save item" title-top="true"></span>\n' +
    '\n' +
    '					<div class="post-menu" ng-if="user && !user.anonymous">\n' +
    '\n' +
    '						<i class="icon-menu" title="More options" title-top="true" ng-click="item.showMenu = !item.showMenu"></i>\n' +
    '\n' +
    '						<ul class="dropdown" ng-if="item.showMenu" click-outside="item.showMenu = false">\n' +
    '							<li ng-if="!item.nsfw && !item.userNSFW"><a href="" ng-click="::markNSFW(item)">Report NSFW</a></li>\n' +
    '							<li ng-if="item.userNSFW"><a href="" ng-click="::unmarkNSFW(item)">Mark as SFW</a></li>\n' +
    '							<li><a href="" ng-click="::markHidden(item)">Hide from My feed</a></li>\n' +
    '						</ul>\n' +
    '					</div>\n' +
    '\n' +
    '					<div class="stats pull-right">\n' +
    '\n' +
    '						<span class="action" ng-click="::openDNlink($event, item.link_out_direct)" ng-if="item.source.name == \'designer_news\' && item.external_url" title="Open discussion" title-top="true">\n' +
    '							<span class="icon-chat"></span>\n' +
    '						</span>\n' +
    '\n' +
    '						<span title="Votes" title-top="true" ng-if="::item.source.name === \'producthunt\'">\n' +
    '							<span class="icon-ph"></span>\n' +
    '							<span>{{::item.stats.likes || 0 | thousandSuffix:1}}</span>\n' +
    '						</span>\n' +
    '\n' +
    '						<span title="Views" title-top="true" ng-if="::item.clicks > 0">\n' +
    '							<span class="icon-view"></span>\n' +
    '							<span>{{::item.clicks || 0 | thousandSuffix:1}}</span>\n' +
    '						</span>\n' +
    '						<span title="Shares" title-top="true">\n' +
    '							<span class="icon-share"></span>\n' +
    '							<span>{{::item.virality || 0 | thousandSuffix:1}}</span>\n' +
    '						</span>\n' +
    '	                </div>\n' +
    '\n' +
    '	            </div>\n' +
    '\n' +
    '			</div>\n' +
    '		</div>\n' +
    '\n' +
    '	</li>\n' +
    '</ul>\n' +
    '\n' +
    '<div ng-show="feed && !feed.length && !blockEmpty && !isRendering" ng-transclude="no-data">\n' +
    '</div>\n' +
    '\n' +
    '<ul class="dummy" ng-show="!feed">\n' +
    '	<li></li>\n' +
    '	<li></li>\n' +
    '	<li></li>\n' +
    '	<li></li>\n' +
    '	<li></li>\n' +
    '	<li></li>\n' +
    '	<li></li>\n' +
    '	<li></li>\n' +
    '	<li></li>\n' +
    '	<li></li>\n' +
    '</ul>\n' +
    '\n' +
    '</section>\n' +
    '')
  $templateCache.put('modules/feed/feed.drv.favorites.html',
    '<section id="feed" class="palettes"><!-- Feed posts -->\n' +
    '\n' +
    '<ul\n' +
    '	infinite-scroll=\'::loadMore()\'\n' +
    '	infinite-scroll-distance=\'::infiniteScrollDistance\'\n' +
    '	infinite-scroll-immediate-check=\'false\'\n' +
    '	infinite-scroll-disabled=\'!feed.length\'>\n' +
    '\n' +
    '	<li feed-item ng-repeat="item in feed track by (item.id || item.link)"\n' +
    '	ng-class="::{\n' +
    '		article: item.source.article && !item.video,\n' +
    '		visited: item.visited,\n' +
    '		viral: !!item.viralTimes,\n' +
    '		hasStats: item.hasStats, pick: item.pick,\n' +
    '		animated: !!item.animated,\n' +
    '		\'inline-video\': !!item.htmlVideo,\n' +
    '		video: !!item.video,\n' +
    '		playing: item.playing,\n' +
    '		vlog: !!item.sub_source,\n' +
    '		nsfw: !!item.nsfw || !!item.userNSFW,\n' +
    '		feedSuggest: !!item.promotion,\n' +
    '		fallbackImage: item.fallbackImage,\n' +
    '		showSharePromo: item.displaySharePromo,\n' +
    '		outside: item.isOutside,\n' +
    '		showMenu: item.showMenu,\n' +
    '		}"\n' +
    '	class="angular-animate"\n' +
    '	data-muzli-id="{{::item.id}}">\n' +
    '\n' +
    '		<!-- Color items -->\n' +
    '		<div class="tile" ng-if="::item.source.name === \'colors\'">\n' +
    '\n' +
    '			<div class="feedLink">\n' +
    '\n' +
    '				<span class="icon-image" ng-if="item.image" ng-click="item.showPhoto = !item.showPhoto"></span>\n' +
    '\n' +
    '				<div class="postPhoto">\n' +
    '					<div class="palette" style="background: {{item.palette[item.palette.length - 1]}}" ng-class="{hidden: item.showPhoto}">\n' +
    '						<span ng-repeat="color in item.palette"\n' +
    '							style="background: {{color}}"\n' +
    '							title-top="true"\n' +
    '							click-copy-color="{{::color}}"\n' +
    '							brightness="{{::color}}"\n' +
    '							prevent-click>\n' +
    '							{{color}}\n' +
    '						</span>\n' +
    '					</div>\n' +
    '					<a class="wrapper" ng-mousedown="::postClick(item, $event)" target="_blank" ng-href="{{::item.link_out}}">\n' +
    '						<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAMAAAAoyzS7AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAAZQTFRFAAAAAAAApWe5zwAAAAF0Uk5TAEDm2GYAAAAMSURBVHjaYmAACDAAAAIAAU9tWeEAAAAASUVORK5CYII=" muzli-lazy="{{::item.image}}" muzli-gif="{{::item.gif}}" muzli-is-gif="{{::item.isGif}}" src="" alt="" />\n' +
    '					</a>\n' +
    '				</div>\n' +
    '\n' +
    '				<div class="postInfo">\n' +
    '					<div class="download" title="Download palette (SVG)" ng-click="downloadSVG(item, $event)">Download</div>\n' +
    '				</div>\n' +
    '\n' +
    '			</div>\n' +
    '\n' +
    '			<div class="postMeta">\n' +
    '\n' +
    '				<div class="details angular-animate">\n' +
    '\n' +
    '					<span class="remove" ng-click="::removeFavorite($event, $index, item)" title="Remove" title-top="true"></span>\n' +
    '\n' +
    '					<div class="stats pull-right">\n' +
    '\n' +
    '						<span class="action" ng-click="::openDNlink($event, item.link_out_direct)" ng-if="item.source.name == \'designer_news\' && item.external_url" title="Open discussion" title-top="true">\n' +
    '							<span class="icon-chat"></span>\n' +
    '						</span>\n' +
    '\n' +
    '						<span title="Votes" title-top="true" ng-if="::item.source.name === \'producthunt\'">\n' +
    '							<span class="icon-ph"></span>\n' +
    '							<span>{{::item.stats.likes || 0 | thousandSuffix:1}}</span>\n' +
    '						</span>\n' +
    '\n' +
    '						<span title="Views" title-top="true" ng-if="::item.clicks > 0">\n' +
    '							<span class="icon-view"></span>\n' +
    '							<span>{{::item.clicks || 0 | thousandSuffix:1}}</span>\n' +
    '						</span>\n' +
    '						<span title="Shares" title-top="true">\n' +
    '							<span class="icon-share"></span>\n' +
    '							<span>{{::item.virality || 0 | thousandSuffix:1}}</span>\n' +
    '						</span>\n' +
    '	                </div>\n' +
    '\n' +
    '	            </div>\n' +
    '\n' +
    '			</div>\n' +
    '		</div>\n' +
    '\n' +
    '		<!-- Simple items -->\n' +
    '		<div class="tile" ng-if="::item.source.name !== \'colors\'">\n' +
    '\n' +
    '			<a target="_blank" ng-href="{{::item.link_out}}" class="feedLink" ng-mousedown="::postClick(item, $event)">\n' +
    '\n' +
    '				<div class="postPhoto" >\n' +
    '\n' +
    '					<i ng-if="item.nsfw || item.userNSFW">NSFW</i>\n' +
    '\n' +
    '					<img ng-if="::!item.video" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAMAAAAoyzS7AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAAZQTFRFAAAAAAAApWe5zwAAAAF0Uk5TAEDm2GYAAAAMSURBVHjaYmAACDAAAAIAAU9tWeEAAAAASUVORK5CYII=" muzli-lazy="{{::item.image}}" muzli-gif="{{::item.gif}}" muzli-is-gif="{{::item.isGif}}" src="" alt="" />\n' +
    '\n' +
    '					<video ng-if="::item.htmlVideo" ng-src="{{::item.htmlVideo}}" video-loader playsinline loop autoplay muted></video>\n' +
    '\n' +
    '					<div class="share">\n' +
    '						<span class="facebook icon-facebook" ng-click="::openSharer($event, \'facebook\', item)" title="Share on Facebook" title-top="true"></span>\n' +
    '						<span class="twitter icon-twitter" ng-click="::openSharer($event, \'twitter\', item)" title="Share on Twitter" title-top="true"></span>\n' +
    '						<span class="linkedin icon-linkedin" ng-click="::openSharer($event, \'linkedin\', item)" title="Share on LinkedIn" title-top="true"></span>\n' +
    '						<span class="slack icon-slack" ng-click="::sendSlack($event, item)" title="Share on Slack" title-bottom="true"></span>\n' +
    '					</div>\n' +
    '\n' +
    '					<muzli-video ng-if="::item.video" ng-click="::videoClick(item, $event)"></muzli-video>\n' +
    '\n' +
    '					<div class="share-promo" ng-if="item.displaySharePromo">\n' +
    '						<div class="sharers">\n' +
    '							<h4>Nice! Maybe your friends will also enjoy this?</h4>\n' +
    '							<span class="facebook icon-facebook" ng-click="::openSharer($event, \'facebook\', item)" title="Share on Facebook"></span>\n' +
    '							<span class="twitter icon-twitter" ng-click="::openSharer($event, \'twitter\', item)" title="Share on Twitter"></span>\n' +
    '							<span class="linkedin icon-linkedin" ng-click="::openSharer($event, \'linkedin\', item)" title="Share on LinkedIn"></span>\n' +
    '							<span class="slack icon-slack" ng-click="::sendSlack($event, item)" title="Share on Slack"></span>\n' +
    '						</div>\n' +
    '					</div>\n' +
    '\n' +
    '					<span class="badge"></span>\n' +
    '				</div>\n' +
    '\n' +
    '				<div class="postInfo">\n' +
    '\n' +
    '					<div class="source-wrapper">\n' +
    '\n' +
    '						<span class="source" title="{{::item.tooltip}}"\n' +
    '						ui-sref="feed(::{ name: item.source.name })"\n' +
    '						ng-click="::sourceClick($event, item.source.name)"\n' +
    '						ng-style="::{\'background-image\': \'url(images/icon_{{item.source.name}}.png)\'}"\n' +
    '						title="::item.source.title"></span>\n' +
    '\n' +
    '						<span class="vlogUser" title="{{::item.sub_source.title}}"\n' +
    '						ng-if="::item.sub_source"\n' +
    '						ng-style="::{\'background-image\': \'url(images/icon_{{item.sub_source.name}}.png)\'}"\n' +
    '						data-name="{{::item.sub_source.title}}"></span>\n' +
    '					</div>\n' +
    '\n' +
    '					<div class="palette" ng-if="::item.palette && !item.source.article && enablePalettes">\n' +
    '\n' +
    '						<div ng-click="downloadSVG(item, $event)" class="icon-download"></div>\n' +
    '\n' +
    '						<span ng-repeat="color in item.palette"\n' +
    '							style="background: {{color}}; border-color: {{color}}"\n' +
    '							title="{{copySuccess ? \'Copied\' : color}}"\n' +
    '							title-top="true"\n' +
    '							click-copy-color="{{::color}}"\n' +
    '							prevent-click>\n' +
    '						</span>\n' +
    '					</div>\n' +
    '\n' +
    '					<h3 ng-bind="::item.title"></h3>\n' +
    '					<span class="created">{{item.created | timeAgo}}</span>\n' +
    '				</div>\n' +
    '			</a>\n' +
    '\n' +
    '			<div class="postMeta">\n' +
    '\n' +
    '				<div class="details angular-animate">\n' +
    '\n' +
    '					<span class="remove" ng-click="::removeFavorite($event, $index, item)" title="Remove" title-top="true"></span>\n' +
    '\n' +
    '					<div class="stats pull-right">\n' +
    '\n' +
    '						<span class="action" ng-click="::openDNlink($event, item.link_out_direct)" ng-if="item.source.name == \'designer_news\' && item.external_url" title="Open discussion" title-top="true">\n' +
    '							<span class="icon-chat"></span>\n' +
    '						</span>\n' +
    '\n' +
    '						<span title="Votes" title-top="true" ng-if="::item.source.name === \'producthunt\'">\n' +
    '							<span class="icon-ph"></span>\n' +
    '							<span>{{::item.stats.likes || 0 | thousandSuffix:1}}</span>\n' +
    '						</span>\n' +
    '\n' +
    '						<span title="Views" title-top="true" ng-if="::item.clicks > 0">\n' +
    '							<span class="icon-view"></span>\n' +
    '							<span>{{::item.clicks || 0 | thousandSuffix:1}}</span>\n' +
    '						</span>\n' +
    '						<span title="Shares" title-top="true">\n' +
    '							<span class="icon-share"></span>\n' +
    '							<span>{{::item.virality || 0 | thousandSuffix:1}}</span>\n' +
    '						</span>\n' +
    '	                </div>\n' +
    '\n' +
    '	            </div>\n' +
    '\n' +
    '			</div>\n' +
    '		</div>\n' +
    '\n' +
    '	</li>\n' +
    '</ul>\n' +
    '\n' +
    '<div ng-show="feed && !feed.length && !blockEmpty && !isRendering" ng-transclude="no-data">\n' +
    '</div>\n' +
    '\n' +
    '<ul class="dummy" ng-show="!feed">\n' +
    '	<li></li>\n' +
    '	<li></li>\n' +
    '	<li></li>\n' +
    '	<li></li>\n' +
    '	<li></li>\n' +
    '	<li></li>\n' +
    '	<li></li>\n' +
    '	<li></li>\n' +
    '	<li></li>\n' +
    '	<li></li>\n' +
    '</ul>\n' +
    '\n' +
    '</section>\n' +
    '')
  $templateCache.put('modules/feed/feed.drv.html',
    '<section id="feed"><!-- Feed posts -->\n' +
    '\n' +
    '<ul\n' +
    '	infinite-scroll=\'::loadMore()\'\n' +
    '	infinite-scroll-distance=\'::infiniteScrollDistance\'\n' +
    '	infinite-scroll-immediate-check=\'false\'\n' +
    '	infinite-scroll-disabled=\'!feed.length\'>\n' +
    '\n' +
    '	<li class="sponsored show" ng-if="feed.length && sponsored">\n' +
    '\n' +
    '		<div class="tile">\n' +
    '			<a target=\'_blank\' class="feedLink" ng-href="{{::sponsored.link}}" ng-click="::promotionClick()">\n' +
    '				<div class="postPhoto">\n' +
    '					<img ng-if="!sponsored.image || !sponsored.active" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAMAAAAoyzS7AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAAZQTFRFAAAAAAAApWe5zwAAAAF0Uk5TAEDm2GYAAAAMSURBVHjaYmAACDAAAAIAAU9tWeEAAAAASUVORK5CYII=" alt=""/>\n' +
    '					<img ng-if="sponsored.image && sponsored.active" ng-src="{{::sponsored.image}}" alt=""/>\n' +
    '					<img class="beacon" style="visibility: hidden;" ng-src="{{::sponsored.beacon}}" ng-if="showAdPixel" alt=""/>\n' +
    '				</div>\n' +
    '				<div class="postInfo">\n' +
    '					<h3>{{::sponsored.name}}</h3>\n' +
    '\n' +
    '				</div>\n' +
    '				<h4>PROMOTED</h4>\n' +
    '			</a>\n' +
    '		</div>\n' +
    '	</li>\n' +
    '\n' +
    '	<li feed-item ng-repeat="item in feed track by (item.id || item.link)"\n' +
    '	ng-class="::{\n' +
    '		article: item.source.article && !item.video,\n' +
    '		visited: item.visited,\n' +
    '		viral: !!item.viralTimes,\n' +
    '		hasStats: item.hasStats, pick: item.pick,\n' +
    '		animated: !!item.animated,\n' +
    '		\'inline-video\': !!item.htmlVideo,\n' +
    '		video: !!item.video,\n' +
    '		playing: item.playing,\n' +
    '		vlog: !!item.sub_source,\n' +
    '		nsfw: !!item.nsfw || !!item.userNSFW,\n' +
    '		feedSuggest: !!item.promotion,\n' +
    '		fallbackImage: item.fallbackImage,\n' +
    '		showSharePromo: item.displaySharePromo,\n' +
    '		outside: item.isOutside,\n' +
    '		showMenu: item.showMenu,\n' +
    '		}"\n' +
    '	class="angular-animate"\n' +
    '	data-muzli-id="{{::item.id}}">\n' +
    '\n' +
    '		<div class="tile">\n' +
    '\n' +
    '			<a target="_blank" ng-href="{{::item.link_out}}" class="feedLink" ng-mousedown="::postClick(item, $event)">\n' +
    '\n' +
    '				<div class="postPhoto" >\n' +
    '\n' +
    '					<i ng-if="item.nsfw || item.userNSFW">NSFW</i>\n' +
    '\n' +
    '					<img ng-if="::!item.video" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAMAAAAoyzS7AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAAZQTFRFAAAAAAAApWe5zwAAAAF0Uk5TAEDm2GYAAAAMSURBVHjaYmAACDAAAAIAAU9tWeEAAAAASUVORK5CYII=" muzli-lazy="{{::item.image}}" muzli-gif="{{::item.gif}}" muzli-is-gif="{{::item.isGif}}" src="" alt="" />\n' +
    '\n' +
    '					<video ng-if="::item.htmlVideo" ng-src="{{::item.htmlVideo}}" video-loader playsinline autoplay loop muted></video>\n' +
    '\n' +
    '					<div class="share">\n' +
    '						<span class="facebook icon-facebook" ng-click="::openSharer($event, \'facebook\', item)" title="Share on Facebook" title-top="true"></span>\n' +
    '						<span class="twitter icon-twitter" ng-click="::openSharer($event, \'twitter\', item)" title="Share on Twitter" title-top="true"></span>\n' +
    '						<span class="linkedin icon-linkedin" ng-click="::openSharer($event, \'linkedin\', item)" title="Share on LinkedIn" title-top="true"></span>\n' +
    '						<span class="slack icon-slack" ng-click="::sendSlack($event, item)" title="Share on Slack" title-bottom="true"></span>\n' +
    '					</div>\n' +
    '\n' +
    '					<muzli-video ng-if="::item.video" ng-click="::videoClick(item, $event)"></muzli-video>\n' +
    '\n' +
    '					<div class="share-promo" ng-if="item.displaySharePromo">\n' +
    '						<div class="sharers">\n' +
    '							<h4>Nice! Maybe your friends will also enjoy this?</h4>\n' +
    '							<span class="facebook icon-facebook" ng-click="::openSharer($event, \'facebook\', item)" title="Share on Facebook"></span>\n' +
    '							<span class="twitter icon-twitter" ng-click="::openSharer($event, \'twitter\', item)" title="Share on Twitter"></span>\n' +
    '							<span class="linkedin icon-linkedin" ng-click="::openSharer($event, \'linkedin\', item)" title="Share on LinkedIn"></span>\n' +
    '							<span class="slack icon-slack" ng-click="::sendSlack($event, item)" title="Share on Slack"></span>\n' +
    '						</div>\n' +
    '					</div>\n' +
    '\n' +
    '					<span class="badge"></span>\n' +
    '\n' +
    '				</div>\n' +
    '\n' +
    '				<div class="postInfo">\n' +
    '\n' +
    '					<div class="source-wrapper">\n' +
    '\n' +
    '						<span class="source" title="{{::item.tooltip}}"\n' +
    '						ui-sref="feed(::{ name: item.source.name })"\n' +
    '						ng-click="::sourceClick($event, item.source.name)"\n' +
    '						ng-style="::{\'background-image\': \'url(images/icon_{{item.source.name}}.png)\'}"\n' +
    '						title="::item.source.title"></span>\n' +
    '\n' +
    '						<span class="vlogUser" title="{{::item.sub_source.title}}"\n' +
    '						ng-if="::item.sub_source"\n' +
    '						ng-style="::{\'background-image\': \'url(images/icon_{{item.sub_source.name}}.png)\'}"\n' +
    '						data-name="{{::item.sub_source.title}}"></span>\n' +
    '\n' +
    '					</div>\n' +
    '\n' +
    '					<div class="palette" ng-if="::item.palette && !item.source.article && enablePalettes">\n' +
    '\n' +
    '						<div ng-click="downloadSVG(item, $event)" class="icon-download" title="Download palette (SVG)"></div>\n' +
    '\n' +
    '						<span ng-repeat="color in item.palette"\n' +
    '							style="background: {{color}}; border-color: {{color}}"\n' +
    '							title="{{copySuccess ? \'Copied\' : color}}"\n' +
    '							title-top="true"\n' +
    '							click-copy-color="{{::color}}"\n' +
    '							prevent-click>\n' +
    '						</span>\n' +
    '					</div>\n' +
    '\n' +
    '					<h3 ng-bind="::item.title"></h3>\n' +
    '					<span class="created">{{item.created | timeAgo}}</span>\n' +
    '				</div>\n' +
    '			</a>\n' +
    '\n' +
    '			<div class="postMeta">\n' +
    '\n' +
    '				<div class="details angular-animate">\n' +
    '\n' +
    '					<span class="favorite icon-fav" ng-click="::toggleFavorite($event, item)" ng-if="::showFavorite" ng-class="{ active: item.favorite }" title="Save item" title-top="true"></span>\n' +
    '\n' +
    '					<div class="post-menu" ng-if="user && !user.anonymous">\n' +
    '\n' +
    '						<i class="icon-menu" title="More options" title-top="true" ng-click="item.showMenu = !item.showMenu"></i>\n' +
    '\n' +
    '						<ul class="dropdown" ng-if="item.showMenu" click-outside="item.showMenu = false">\n' +
    '							<li ng-if="!item.nsfw && !item.userNSFW"><a href="" ng-click="::markNSFW(item)">Report NSFW</a></li>\n' +
    '							<li ng-if="item.userNSFW"><a href="" ng-click="::unmarkNSFW(item)">Mark as SFW</a></li>\n' +
    '							<li><a href="" ng-click="::markHidden(item)">Hide from My feed</a></li>\n' +
    '						</ul>\n' +
    '					</div>\n' +
    '\n' +
    '					<div class="stats pull-right">\n' +
    '\n' +
    '						<span class="action" ng-click="::openDNlink($event, item.link_out_direct)" ng-if="item.source.name == \'designer_news\' && item.external_url" title="Open discussion" title-top="true">\n' +
    '							<span class="icon-chat"></span>\n' +
    '						</span>\n' +
    '\n' +
    '						<span title="Votes" title-top="true" ng-if="::item.source.name === \'producthunt\'">\n' +
    '							<span class="icon-ph"></span>\n' +
    '							<span>{{::item.stats.likes || 0 | thousandSuffix:1}}</span>\n' +
    '						</span>\n' +
    '\n' +
    '						<span title="Views" title-top="true" ng-if="::item.clicks > 0">\n' +
    '							<span class="icon-view"></span>\n' +
    '							<span>{{::item.clicks || 0 | thousandSuffix:1}}</span>\n' +
    '						</span>\n' +
    '						<span title="Shares" title-top="true">\n' +
    '							<span class="icon-share"></span>\n' +
    '							<span>{{::item.virality || 0 | thousandSuffix:1}}</span>\n' +
    '						</span>\n' +
    '	                </div>\n' +
    '\n' +
    '	            </div>\n' +
    '\n' +
    '			</div>\n' +
    '		</div>\n' +
    '\n' +
    '	</li>\n' +
    '</ul>\n' +
    '\n' +
    '<div ng-show="feed && !feed.length && !blockEmpty && !isRendering" ng-transclude="no-data">\n' +
    '</div>\n' +
    '\n' +
    '<ul class="dummy" ng-show="!feed">\n' +
    '	<li></li>\n' +
    '	<li></li>\n' +
    '	<li></li>\n' +
    '	<li></li>\n' +
    '	<li></li>\n' +
    '	<li></li>\n' +
    '	<li></li>\n' +
    '	<li></li>\n' +
    '	<li></li>\n' +
    '	<li></li>\n' +
    '</ul>\n' +
    '\n' +
    '</section>\n' +
    '')
  $templateCache.put('modules/feed/feed.html',
    '<div class="cf">\n' +
    '\n' +
    '  <h2 ng-cloak ng-if="currentSource.name">\n' +
    '    <span title="{{currentSource.description}}">{{currentSource.title}}</span>\n' +
    '    <a ng-if="[\'vlogs\'].indexOf(currentSource.name) === -1" title="Go to {{currentSource.title}}\'s website" href="http://app.muz.li/go?link=http://{{currentSource.url}}" class="titleLink"></a>\n' +
    '  </h2>\n' +
    '\n' +
    '  <div class="feedSorters pull-right" ng-if="[\'search\', \'favorites\'].indexOf(currentSource) === -1 && [\'vlogs\', \'dribbble\', \'producthunt\', \'colors\'].indexOf(currentSource.name) === -1" ng-cloak>\n' +
    '    <a ng-click="::sortFeed(\'virality\')" ng-class="{ active: currentFeedSort === \'virality\' }" href="" title="Sort by popularity">POPULAR</a>\n' +
    '    <a ng-click="::sortFeed()"ng-class="{ active: currentFeedSort !== \'virality\' }" href="" title="Sort by date">RECENT</a>\n' +
    '  </div>\n' +
    '\n' +
    '  <div class="colors-cta" ng-if="currentSource.name === \'colors\'">\n' +
    '    <a href="https://search.muz.li/color-palette-generator"><i class="icon-plus"></i></a>\n' +
    '  </div>\n' +
    '\n' +
    '</div>\n' +
    '\n' +
    '<scrollable-feed items="items" ng-hide="errors.length" sponsored="sponsored" load-on-sort show-favorite show-virality>\n' +
    '  <scrollable-feed-no-data>\n' +
    '    <div id="oops">No items yet today <a href="" ng-click="::reload()">Try again</a>.</div>\n' +
    '  </scrollable-feed-no-data>\n' +
    '</scrollable-feed>\n' +
    '')
  $templateCache.put('modules/feed/user-sources.html',
    '<div class="cf">\n' +
    '\n' +
    '  <h2>All your feeds</h2>\n' +
    '\n' +
    '  <div class="feedSorters pull-right" ng-if="[\'search\', \'favorites\'].indexOf(currentSource) === -1 && [\'vlogs\', \'dribbble\', \'producthunt\'].indexOf(currentSource.name) === -1" ng-cloak>\n' +
    '    <a ng-click="::sortFeed(\'virality\')" ng-class="{ active: currentFeedSort === \'virality\' }" href="" title="Sort by popularity">POPULAR</a>\n' +
    '    <a ng-click="::sortFeed()"ng-class="{ active: currentFeedSort !== \'virality\' }" href="" title="Sort by date">RECENT</a>\n' +
    '  </div>\n' +
    '\n' +
    '</div>\n' +
    '\n' +
    '<scrollable-feed items="items" sponsored="sponsored" ng-hide="errors.length" load-on-sort="viralTimes" show-favorite show-virality></scrollable-feed>\n' +
    '')
  $templateCache.put('modules/feed/video.drv.html',
    '<div ng-click="::imageClicked($event)" class="thumbnail angular-youtube-wrapper">\n' +
    '	<img ng-if="item.thumbnail" muzli-lazy="{{::item.thumbnail}}">\n' +
    '	<div ng-if="item.thumbnail" class="player-image"></div>\n' +
    '</div>')
  $templateCache.put('modules/muzli/card.drv.html',
    '<div class="tile">\n' +
    '\n' +
    '  <a target="_blank" ng-href="{{::item.link_out}}" class="feedLink" ng-mousedown="::postClick(item, $event)" >\n' +
    '    <div class="postPhoto">\n' +
    '\n' +
    '      <i ng-if="item.nsfw || item.userNSFW">NSFW</i>\n' +
    '\n' +
    '      <img ng-if="::!item.video && !item.youtube" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAMAAAAoyzS7AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAAZQTFRFAAAAAAAApWe5zwAAAAF0Uk5TAEDm2GYAAAAMSURBVHjaYmAACDAAAAIAAU9tWeEAAAAASUVORK5CYII=" muzli-lazy="{{::item.image}}" muzli-gif="{{::item.gif}}" muzli-is-gif="{{::item.isGif}}" alt="" />\n' +
    '\n' +
    '      <div class="share">\n' +
    '        <span class="facebook icon-facebook" ng-click="::openSharer($event, \'facebook\', item)" title="Share on Facebook" title-top="true"></span>\n' +
    '        <span class="twitter icon-twitter" ng-click="::openSharer($event, \'twitter\', item)" title="Share on Twitter" title-top="true"></span>\n' +
    '        <span class="linkedin icon-linkedin" ng-click="::openSharer($event, \'linkedin\', item)" title="Share on LinkedIn" title-top="true"></span>\n' +
    '        <span class="slack icon-slack" ng-click="::sendSlack($event, item)" title="Share on Slack" title-bottom="true"></span>\n' +
    '      </div>\n' +
    '\n' +
    '      <muzli-video ng-if="::item.video"></muzli-video>\n' +
    '\n' +
    '      <span class="badge"></span>\n' +
    '\n' +
    '      <div class="share-promo" ng-if="item.displaySharePromo">\n' +
    '        <div class="sharers">\n' +
    '          <h4>Nice! Maybe your friends will also enjoy this?</h4>\n' +
    '          <span class="facebook icon-facebook" ng-click="::openSharer($event, \'facebook\', item)" title="Share on Facebook"></span>\n' +
    '          <span class="twitter icon-twitter" ng-click="::openSharer($event, \'twitter\', item)" title="Share on Twitter"></span>\n' +
    '          <span class="linkedin icon-linkedin" ng-click="::openSharer($event, \'linkedin\', item)" title="Share on LinkedIn"></span>\n' +
    '          <span class="slack icon-slack" ng-click="::sendSlack($event, item)" title="Share on Slack"></span>\n' +
    '        </div>\n' +
    '      </div>\n' +
    '\n' +
    '    </div>\n' +
    '  </a>\n' +
    '\n' +
    '  <div class="postMeta">\n' +
    '\n' +
    '    <div class="postInfo">\n' +
    '      <h3 ng-bind="::item.title"></h3>\n' +
    '      <span class="source" ng-bind="::item.source.title"></span>\n' +
    '    </div>\n' +
    '\n' +
    '    <div class="details angular-animate">\n' +
    '\n' +
    '      <span class="favorite icon-fav" ng-click="::toggleFavorite($event, item)" ng-class="{ active: item.favorite }" title="Save item" title-top="true"></span>\n' +
    '\n' +
    '      <div class="post-menu" ng-if="user && !user.anonymous">\n' +
    '\n' +
    '        <i class="icon-menu" title="More options" title-top="true" ng-click="item.showMenu = !item.showMenu"></i>\n' +
    '\n' +
    '        <ul class="dropdown" ng-if="item.showMenu" click-outside="item.showMenu = false">\n' +
    '          <li ng-if="!item.nsfw && !item.userNSFW"><a href="" ng-click="::markNSFW(item)">Report NSFW</a></li>\n' +
    '          <li ng-if="item.userNSFW"><a href="" ng-click="::unmarkNSFW(item)">Mark as SFW</a></li>\n' +
    '          <li><a href="" ng-click="::markHidden(item)">Hide from My feed</a></li>\n' +
    '        </ul>\n' +
    '      </div>\n' +
    '\n' +
    '      <div class="stats pull-right">\n' +
    '        <span class="action" ng-click="::openDNlink($event, item.link_out_direct)" ng-if="item.source.name == \'designer_news\' && item.external_url" title="Open discussion" title-top="true">\n' +
    '          <span class="icon-chat"></span>\n' +
    '        </span>\n' +
    '\n' +
    '        <span title="Votes" title-top="true" ng-if="::item.source.name === \'producthunt\'">\n' +
    '          <span class="icon-ph"></span>\n' +
    '          <span>{{::item.stats.likes || 0 | thousandSuffix:1}}</span>\n' +
    '        </span>\n' +
    '\n' +
    '        <span title="Views" title-top="true" ng-if="::item.clicks > 0">\n' +
    '          <span class="icon-view"></span>\n' +
    '          <span>{{::item.clicks || 0 | thousandSuffix:1}}</span>\n' +
    '        </span>\n' +
    '        <span title="Shares" title-top="true">\n' +
    '          <span class="icon-share"></span>\n' +
    '          <span>{{::item.virality || 0 | thousandSuffix:1}}</span>\n' +
    '        </span>\n' +
    '      </div>\n' +
    '\n' +
    '    </div>\n' +
    '  </div>\n' +
    '</div>\n' +
    '')
  $templateCache.put('modules/muzli/home.html',
    '<div class="home-overlay cf angular-animate"\n' +
    '	ng-hide="errors.length"\n' +
    '	ng-if="!feedVisibleClass"\n' +
    '	ng-class="{show: areHomeImagesLoaded}">\n' +
    '\n' +
    '	<section id="quickAccess" class="max minimized angular-animate" ng-if="!feedVisibleClass" ng-class="{\n' +
    '		\'min\': showEnableAuthRecentSites === false && !recentSites,\n' +
    '		\'max\': showEnableAuthRecentSites || recentSites,\n' +
    '		\'visible\': !isSwitchedToHalfView && feedVisibleClass !== \'halfView\'\n' +
    '		}">\n' +
    '		<div class="search">\n' +
    '			<form ng-submit="::search(searchText, $event)" id="searchForm">\n' +
    '				<div class="input {{defaultSearch}}">\n' +
    '					<input tabindex="1" maxlength="50" type="text" ng-model="searchText" id="searchBox" placeholder="Search Google or Muzli" name="q" lookahead ng-class="{active: showMuzliSearchPrompt}"/>\n' +
    '					<div class="searchOptions">\n' +
    '						<i class="icon-search"></i>\n' +
    '						<a href="" ng-class="{ default: defaultSearch === \'muzli\', active: activeSearch === \'muzli\' }" class="muzli icon-muzli" title="Muzli search" ng-click="search(searchText, $event, \'muzli\')"></a>\n' +
    '						<a href="" ng-class="{ default: defaultSearch === \'google\', active: activeSearch === \'google\' }" class="google icon-google" title="Google search" ng-click="search(searchText, $event, \'google\')"></a>\n' +
    '					</div>\n' +
    '\n' +
    '				</div>\n' +
    '			</form>\n' +
    '\n' +
    '			<div class="placeholder" ng-if="showMuzliSearchPrompt">\n' +
    '	          <div class="container">\n' +
    '\n' +
    '	            <div class="enableMessage">\n' +
    '	              <p>Search by color, keyword, or even just style with the <strong> new Muzli Search</strong>- a web-wide search for design inspiration.</p>\n' +
    '	              <span class="privacy">You can always switch back to Google search from your settings.</span>\n' +
    '	            </div>\n' +
    '\n' +
    '	            <a href="" class="cta" ng-click="::enableMuzliPrompt()">Use inspiration search</a>\n' +
    '	            <a href="" class="close" ng-click="::closeMuzliPrompt()">maybe later</a>\n' +
    '	          </div>\n' +
    '	        </div>\n' +
    '\n' +
    '		</div>\n' +
    '		<div class="clock">\n' +
    '			<span class="time">{{currentTime | date:\'h:mm a\'}}</span>\n' +
    '			<span class="date">{{currentTime | date:\'EEEE, MMMM d\'}}</span>\n' +
    '		</div>\n' +
    '	</section>\n' +
    '\n' +
    '	<section id="sticky">\n' +
    '		<ul class="home">\n' +
    '			<li ng-repeat="item in muzliFeed track by (item.id || item.link)"\n' +
    '				ng-class="::{\n' +
    '					viral: item.viral,\n' +
    '					video: !!item.video,\n' +
    '					playing: item.playing,\n' +
    '					animated: !!item.animated,\n' +
    '					nsfw: !!item.nsfw || !!item.userNSFW,\n' +
    '					showSharePromo: item.displaySharePromo,\n' +
    '					showMenu: item.showMenu,\n' +
    '					hidden: item.userHidden,\n' +
    '					}"\n' +
    '				data-muzli-id="{{::item.id}}"\n' +
    '				ng-include="\'modules/muzli/card.drv.html\'"\n' +
    '				></li>\n' +
    '		</ul>\n' +
    '	</section>\n' +
    '\n' +
    '</div>\n' +
    '\n' +
    '<div class="home-content angular-animate"\n' +
    '	ng-hide="!feedVisibleClass"\n' +
    '	ng-hide="errors.indexOf(\'all-error\') > -1"\n' +
    '	ng-if="areHomeImagesLoaded || feedVisibleClass">\n' +
    '\n' +
    '	<div class="our-picks">\n' +
    '\n' +
    '		<div class="cf">\n' +
    '			<h2 class="latest-stories">From our picks</h2>\n' +
    '		</div>\n' +
    '\n' +
    '		<section id="sticky">\n' +
    '\n' +
    '			<!-- Sticky posts -->\n' +
    '			<ul class="picks" ng-if="muzliFeed.length">\n' +
    '\n' +
    '				<li ng-repeat="item in muzliFeed | filterHome track by (item.id || item.link)" ng-class="::{\n' +
    '				viral: item.viral,\n' +
    '				video: !!item.video,\n' +
    '				playing: item.playing,\n' +
    '				animated: !!item.animated,\n' +
    '				nsfw: !!item.nsfw || !!item.userNSFW,\n' +
    '				showSharePromo: item.displaySharePromo,\n' +
    '				showMenu: item.showMenu,\n' +
    '				hidden: item.userHidden,\n' +
    '				}"\n' +
    '				class="angular-animate"\n' +
    '				data-muzli-id="{{::item.id}}"\n' +
    '				ng-include="\'modules/muzli/card.drv.html\'"></li>\n' +
    '			</ul>\n' +
    '\n' +
    '		</section>\n' +
    '	</div>\n' +
    '\n' +
    '	<div class="cf" ng-if="allFeed.length" ng-cloak>\n' +
    '		<h2 class="latest-stories pull-left" ng-hide="errors.indexOf(\'all-error\') > -1">Latest Stories</h2>\n' +
    '		<div class="feedSorters pull-right">\n' +
    '			<a ng-click="::sortFeed(\'virality\')" ng-class="{ active: currentFeedSort === \'virality\' }" href="" title="Sort by popularity">POPULAR</a>\n' +
    '    		<a ng-click="::sortFeed()"ng-class="{ active: currentFeedSort !== \'virality\' }" href="" title="Sort by date">RECENT</a>\n' +
    '		</div>\n' +
    '	</div>\n' +
    '\n' +
    '	<scrollable-feed items="allFeed" sponsored="sponsored" ng-hide="errors.length" load-on-sort="viralTimes" show-favorite show-virality></scrollable-feed>\n' +
    '</div>\n' +
    '')
  $templateCache.put('modules/muzli/version-info.html',
    '<div class="pull-left version-info" ng-cloak>\n' +
    '	<div class="pull-left">\n' +
    '		<p>Environment: <strong>{{currentEnvironment}}</strong> | \n' +
    '			<a ng-repeat="environment in environments" href="" ng-click="setEnvironment(environment)">{{environment}}</a> \n' +
    '		</p>\n' +
    '		<p>API version: <strong>{{user.apiVersion}}</strong></p>\n' +
    '	</div>\n' +
    '	<div class="pull-left">\n' +
    '		<p>FTX:&nbsp;&nbsp;</p>\n' +
    '	</div>\n' +
    '	<div class="pull-left">\n' +
    '		<p>\n' +
    '			<a href="" ui-sref="welcome"> Trigger FTX</a>\n' +
    '			<a href="" ui-sref="safari"> Safari FTX</a>\n' +
    '			Post-config steps:\n' +
    '			<a href="" ng-click="vm.ftxStep = 1">1</a>\n' +
    '			<a href="" ng-click="vm.ftxStep = 2">2</a>\n' +
    '			<a href="" ng-click="vm.ftxStep = 3">3</a>\n' +
    '			<a href="" ng-click="vm.ftxStep = 4">4</a>\n' +
    '			<a href="" ng-click="vm.showFtxLast = true; vm.ftxStep = false">Final</a>\n' +
    '		</p>\n' +
    '		<p>\n' +
    '			<a href="" ng-click="vm.showWelcomeOld = true"> Trigger FTX (old)</a>\n' +
    '			<a href="" ui-sref="source-config({sortByNew: true})"> Show sources</a>\n' +
    '		</p>\n' +
    '	</div>\n' +
    '</div>')
  $templateCache.put('modules/search/search.html',
    '<div class="cf">\n' +
    '  <h2 class="resultsHeading">Found {{items.length}} results for <span>{{searchText}}</span></h2>\n' +
    '</div>\n' +
    '\n' +
    '<scrollable-feed items="items" ng-hide="errors.length" show-favorite>\n' +
    '  <scrollable-feed-no-data>\n' +
    '    <div id="oops" class="emptySearch">Nothing found</div>\n' +
    '  </scrollable-feed-no-data>\n' +
    '</scrollable-feed>\n' +
    '')
  $templateCache.put('modules/sources/welcome-safari.html',
    '<div class="welcome safari">\n' +
    '	<div class="split">\n' +
    '		<video src="https://files.muzli.space/muzli5.mp4" autoplay>\n' +
    '	</div>\n' +
    '	<div class="split">\n' +
    '		<h2>Make muzli your Homepage</h2>\n' +
    '		<p>\n' +
    '			Muzli by InVision is designed to serve as your Homepage! <br>\n' +
    '			To get the full experience, you need to set <strong>https://safari.muz.li/</strong> as your default Homepage manually, by following instructions.\n' +
    '		</p>\n' +
    '		<ul>\n' +
    '			<li>Click <strong>“Safari" ⇒ "Preferences”</strong> in the menu on the top-left corner of the screen.</li>\n' +
    '			<li>Select the <strong>“General”</strong> tab on the Preferences pop up window.</li>\n' +
    '			<li>Choose <strong>"Homepage"</strong> in the “New windows open with” drop down.</li>\n' +
    '			<li>Choose <strong>"Homepage"</strong> in the “New tab open with” drop down.</li>\n' +
    '			<li>Type <strong click-copy="https://safari.muz.li/" title="{{copySuccess ? \'Copied\' : \'Copy to clipboard\'}}" title-top="true">https://safari.muz.li/ <i class="icon-copy"></i></strong> in the “Homepage” text box and press ⏎ Return.</li>\n' +
    '			<li>Close the preferences window, and <strong>open a New tab</strong></li>\n' +
    '		</ul>\n' +
    '\n' +
    '		<button class="cta" ng-click="$state.go(\'source-config\', {ftx: true})">Got it!</button>\n' +
    '		<a href="" ng-click="$state.go(\'source-config\', {ftx: true})">I\'ll do it later</a>\n' +
    '	</div>\n' +
    '</div>\n' +
    '')
  $templateCache.put('modules/sources/welcome.html',
    '<div class="welcome">\n' +
    '	<i class="icon-muzli"></i>\n' +
    '	<h2>Welcome to your new tab!</h2>\n' +
    '	<p>Muzli by InVision is new-tab, design-oriented magazine that surfaces the most inspiring and engaging content from all your favorite websites.</p>\n' +
    '	<button class="cta" ng-click="$state.go(\'source-config\', {ftx: true})">let\'s go</button>\n' +
    '	<div class="bg"></div>\n' +
    '</div>\n' +
    '')
  $templateCache.put('modules/user/favorites.html',
    '<div class="cf">\n' +
    '  <h2>Your Saved Items <span ng-show="user && user.favoriteCount"></h2>\n' +
    '    <a href="" ng-hide="showFavoritesClearAll" class="clearAllStarred pull-right" ng-click="showFavoritesClearAll = true">Delete all</a>\n' +
    '    <div class="confirm pull-right" ng-show="showFavoritesClearAll">Are you sure you want to delete all your saved items? <span class="true" ng-click="clearAllFavorites()">Yes</span> or <span class="false" ng-click="showFavoritesClearAll = false">Cancel</span></div></span>\n' +
    '</div>\n' +
    '\n' +
    '<scrollable-feed items="items" ng-hide="errors.length" load-on-sort check-delete-length>\n' +
    '  <scrollable-feed-no-data>\n' +
    '    <div class="noFavs">\n' +
    '      <h5>You have no items you like yet</h5>\n' +
    '    </div>\n' +
    '  </scrollable-feed-no-data>\n' +
    '</scrollable-feed>\n' +
    '')

  }]);
