(function() {

    service.$inject = ['$q', 'userService', 'storage'];

    function service($q, userService, storage) {

        return {
            isExtension: window.chrome && !!chrome.extension,
            sendMessage: function(message, callback) {

                if (!window.chrome || !chrome.runtime) {

                    if (callback) {
                        callback()
                    }

                    return;
                }

                chrome.runtime.sendMessage(message, callback);
            }
        }

    }

    angular.module('chrome')
        .factory('chrome', service);

})();