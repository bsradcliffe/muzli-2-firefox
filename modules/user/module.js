(function() {

    favoritesController.$inject = ['$scope', '$rootScope', 'feedFetcher', 'userService', 'trackService'];

    function favoritesController($scope, $rootScope, feedFetcher, userService, trackService) {

        window.muzli.pageChange();
        $rootScope.initScrollTracking($scope);
        $rootScope.areHomeImagesLoaded = true;

        trackService.track({
            category: 'Sidebar',
            action: 'Click',
            label: 'Source: favorites'
        });

        $rootScope.feedVisibleClass = 'halfView';
        $rootScope.initialLoading = 'loading-complete';

        feedFetcher.fetch('favorites', 300).then(function(res) {
            $scope.items = res.data;
        }).catch(function(error) {
            console.error(error);
            $rootScope.setError($scope, 'error');
        });

        $rootScope.clearAllFavorites = function() {
            userService.clearFavorites();
            $scope.items = [];
            $rootScope.user.favoriteCount = 0;
        }
    }

    config.$inject = ['$httpProvider', '$stateProvider'];

    function config($httpProvider, $stateProvider) {

        $stateProvider.state('favorites', {
            templateUrl: 'modules/user/favorites.html',
            controller: favoritesController
        });

        $httpProvider.interceptors.push(['$q', 'server', '$rootScope', '$injector', 'storage',
          function($q, server, $rootScope, $injector, storage) {

            var token = storage.getSync('token');

            return {

                request: function(config) {

                    //Force V2 for color feed
                    if (config.url.indexOf('/feed/colors') !== -1) {
                        config.url = config.url.replace(window.MUZLI_SERVER, window.MUZLI_SERVER_V2)
                    }

                    if (config.skipAuth || !token || config.url.indexOf(server()) !== 0) {
                        return config;
                    }

                    if (config.method === "GET") {
                        config.params = config.params || {};
                        config.params.Authorization = 'Bearer ' + token;
                    } else {
                        config.headers.Authorization = 'Bearer ' + token;
                    }

                    return config;
                },

                responseError: function(rejection) {

                    if (rejection.status === 404) {
                        return $q.reject(rejection);
                    }

                    //Falback to V1 API if it fails
                    if (rejection.config.url.indexOf(window.MUZLI_SERVER_V2) === 0 && window.MUZLI_SERVER_V1 && rejection.status !== -1 && !rejection.config.isFallback) {

                        console.warn('V2 API request failed. Falling back to V1.')
                        window.MUZLI_SERVER = window.MUZLI_SERVER_V1

                        rejection.config.url = rejection.config.url.replace(window.MUZLI_SERVER_V2, window.MUZLI_SERVER)
                        rejection.config.isFallback = true;

                        var $http = $injector.get('$http');
                        return $http(rejection.config);
                    }
                    
                    if (rejection.status === 401) {
                        $rootScope.$broadcast('http:401');
                    }

                    return $q.reject(rejection);
                }
            };
        }]);
    }

    run.$inject = ['$state', 'trackService', 'userService', '$rootScope', '$timeout', '$q', 'storage', 'socialService'];

    function run($state, trackService, userService, $rootScope, $timeout, $q, storage, socialService) {

        $rootScope.resolveUser = $q.defer();
        $rootScope.vm = $rootScope.vm || {};
        $rootScope.vm.showSignInDialog = false;

        /*=============================================
        =            Resolve user from API            =
        =============================================*/

        //Set user from local storage to render UI more fluently
        try {
            $rootScope.user = JSON.parse(localStorage.getItem('user'));
        } catch (e) {
            $rootScope.user = {}
        }

        //Set Different API endpont if user is in V2 bucket
        if ($rootScope.user && $rootScope.user.apiVersion === 'v2') {
            
            window.MUZLI_SERVER_V1 = window.MUZLI_SERVER;
            window.MUZLI_SERVER = window.MUZLI_SERVER_V2 || window.MUZLI_SERVER_V1;

            window.MUZLI_AD_SERVER = window.MUZLI_AD_SERVER_V2 || window.MUZLI_AD_SERVER;
            window.MUZLI_SHARE_SERVER = window.MUZLI_SHARE_SERVER_V2 || window.MUZLI_SHARE_SERVER;

            if (window.resolveAdminServer) {
                window.resolveAdminServer('v2')
            }
            
        } else {
            if (window.resolveAdminServer) {
                window.resolveAdminServer('v1')
            }
        }

        userService.fetch().then(function(user) {

            if (user) {

                $rootScope.user = user;

                localStorage.setItem('user', JSON.stringify($rootScope.user));

                $rootScope.resolveUser.resolve(user);
                $rootScope.isUserResolved = true;

                if (user.provider && user.provider.toLowerCase() === 'facebook' && !user.email) {
                    $rootScope.$broadcast('userError', 'missing_email');
                } else if (window.REGISTERED === 'ftx') {
                    if (window.isMuzliSafari) {
                        $rootScope.vm.ftxStep = 2
                    } else {
                        $rootScope.vm.ftxStep = 1
                    }
                } else if (window.REGISTERED === 'favorite') {
                    $state.go('favorites');
                } else
                if (window.REGISTERED) {
                    $rootScope.vm.showThanksYouSignInDialog = true;
                }
            }

        }).catch(function(anonymousUser) {

            $rootScope.user = anonymousUser;
            localStorage.setItem('user', JSON.stringify($rootScope.user));

            $rootScope.resolveUser.resolve(anonymousUser);
            $rootScope.isUserResolved = true;

            //Trigger FTX
            storage.get(['showedBundles']).then(function(res) {

                //Don't show FTX for lite
                if ($rootScope.isLiteVersion) {
                    return;
                }

                if (!res.showedBundles) {

                    //Set default search to Muzli for a new user
                    $rootScope.setDefaultSearch('muzli');

                    storage.set({
                        showedBundles: true
                    }).then(function() {

                        if ($rootScope.user.onboarding === 'new') {

                            if (window.isMuzliSafari) {
                                $state.go('safari')
                            } else {
                                $state.go('welcome')
                            }

                            trackService.setDimension('dimension3', 'new');
                        } else {
                            $rootScope.vm.showWelcomeOld = true
                            trackService.setDimension('dimension3', 'old');
                        }

                    });

                }
            });

            return userService.checkPromoteLogin()
            .then(function(timeStamp) {
                $timeout(function() {
                    if (!$rootScope.vm.showSignInDialog) {
                        storage.set({
                            'last_prompt_login': timeStamp
                        });
                        $rootScope.vm.showSignInDialog = true;
                    }
                }, 7000);
            })
            .catch(function(err){
                
            })

        })



        /*======================================
        =            User functions            =
        ======================================*/

        $rootScope.shareAfterLogin = function(channel) {

            socialService.share(channel, {
                'twitter': 'http://bit.ly/1MpxFig',
                'facebook': 'http://bit.ly/1qj6Em5',
                'linkedin': 'http://bit.ly/1Q32LqE'
            }[channel], "Lovin' Muzli! Design inspiration on tap. Check it out.");

            trackService.track({
                category: 'Share Promote Dialog',
                action: channel,
                label: 'https://muz.li/'
            });
        };

        $rootScope.clickOutsideBubble = function() {
            if ($rootScope.userError) {
                $rootScope.hideUserError = true;
            }
        };

        $rootScope.signIn = function() {
            $rootScope.vm.showSignInDialog = true;

            trackService.track({
                category: 'SignIn',
                action: 'Button Click'
            });
        };

        $rootScope.clickUser = function() {
            
            var user = $rootScope.user;

            if (user.provider && user.provider.toLowerCase() === 'facebook' && !user.email) {
                userService.rerequest();
            } else {
                $state.go('favorites', {}, { reload: true });
            }
        };

        $rootScope.logOut = function() {
            userService.logOut();
        };

        $rootScope.loginFacebook = function(from) {

            if ($rootScope.hive.ftx) {
                trackService.trackPageView('/ftx/login', 'Log In Facebook');
            } else if (from) {
                trackService.track({
                    category: 'SignIn',
                    action: from,
                    label: 'facebook'
                });
            }

            userService.login('facebook');
        };

        $rootScope.loginTwitter = function(from) {

            if ($rootScope.hive.ftx) {
                trackService.trackPageView('/ftx/login', 'Log In Twitter');
            } else if (from) {
                trackService.track({
                    category: 'SignIn',
                    action: from,
                    label: 'twitter'
                });
            }
            userService.login('twitter');
        };

        $rootScope.loginGoogle = function(from) {

            if ($rootScope.hive.ftx) {
                trackService.trackPageView('/ftx/login', 'Log In Google');
            } else if (from) {
                trackService.track({
                    category: 'SignIn',
                    action: from,
                    label: 'google'
                });
            }

            userService.login('google');
        };

        function syncLocalToUserData() {

            userService.getData().then(function(user) {

                $('body').removeClass('theme');
                $rootScope.theme = user.theme || 'white';
                $rootScope.homeSwitched = user.homeSwitched;

                //If no enableSharebleLinks is set, default to true;
                if (user.enableSharebleLinks === undefined) {
                    user.enableSharebleLinks = true;
                }

                //If no enablePalettes is set, default to true;
                if (user.enablePalettes === undefined) {
                    user.enablePalettes = true;
                }
                
                $rootScope.enableSharebleLinks = user.enableSharebleLinks;
                localStorage.enableSharebleLinks = user.enableSharebleLinks;


                $rootScope.enablePalettes = user.enablePalettes;
                localStorage.enablePalettes = user.enablePalettes;
                
                var update = {};

                if (user.halfView != null) {
                    update.halfView = user.halfView;
                }

                if (user.homeSwitched != null) {
                    update.homeSwitched = user.homeSwitched;
                }

                if (user.theme != null) {
                    update.theme = user.theme;
                }

                if (user.default_search != null) {
                    update.default_search = user.default_search;
                }

                if (user.topSitesDisabled != null) {
                    update.topSitesDisabled = user.topSitesDisabled;
                }

                storage.set(update).then(function() {

                    if (update.halfView != null) {
                        $rootScope.$broadcast('muzliSetView');
                    }

                    if (update.topSitesDisabled != null) {
                        $rootScope.$broadcast('muzliLoadSites');
                    }
                })

            });
        }

        syncLocalToUserData();
    }

    angular.module('user', []).config(config).run(run);

})();
