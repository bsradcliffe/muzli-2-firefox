(function() {

    var details = window.muzli.getDetails();

    trackService.$inject = ['$window'];

    function trackService($window) {

        var guidPromise;

        return {
            getGuid: getGuid,
            onLoad: onLoad,
            track: track,
            trackPageView: trackPageView,
            trackError: trackError,
            setDimension: setDimension,
        };

        function getGuid(storage) {
            guidPromise = guidPromise || storage.get("UUID").then(function(obj) {
                var uuid = obj.UUID;
                if (!uuid) {
                    uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                        var r = crypto.getRandomValues(new Uint8Array(1))[0] % 16 | 0,
                            v = c == 'x' ? r : (r & 0x3 | 0x8);
                        return v.toString(16);
                    });

                    storage.set({ 'UUID': uuid });
                }
                return uuid;
            });

            return guidPromise;
        }

        function onLoad(storage, sources_list) {

            getGuid(storage).then(function(uuid) {
                _sendGAOnLoad({ label: 'User ID', value: uuid });
            });

            if (!$window.localStorage.getItem('sentSideBarTacking')) {
                $window.localStorage.setItem('sentSideBarTacking', "yes");
                sources_list.forEach(function(source) {
                    _sendGA({
                        category: 'Sidebar',
                        action: 'Change',
                        label: 'Source: ' + source.name,
                        value: 1
                    });
                });
            }
        }

        function track(config) {
            _sendGA(config);
        }

        function trackPageView(url, title, callback) {

            if (!window.ga) {
                return;
            }

            window.ga('send', {
                'hitType': 'pageview',
                'page': url,
                'title': title,
                'hitCallback': callback || function() {},
            });
        };

        function setDimension(dimension, value) {

            if (dimension === 'dimension1') {
                console.error('Error: dimension1 is reserved for version tracking only');
                return;
            }

            try {
                window.ga('set', dimension, value);
            } catch (err) {
                console.error("Google analytics error", err);
            }
        }

        function trackError(exception, log) {
            if (log) {
                console.error(exception);
            }

            return true;
        }

        function _sendGA(config) {

            var category = config.category;
            var action = config.action;
            var label = config.label;
            var value = config.value;

            if (value) {
                ga('send', 'event', category, action, label, value);
            } else {
                ga('send', 'event', category, action, label);
            }
        }

        function _sendGAOnLoad(config) {
            config.category = 'App';
            config.action = 'Load';
            _sendGA(config);
        }

    }

    angular.module('muzli').factory('trackService', trackService);

})();