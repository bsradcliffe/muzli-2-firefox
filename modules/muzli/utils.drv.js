(function() {

    function muzliTooltip($timeout) {

        function hideToolTip(jEl) {
            var tooltip = jEl.data('tooltipsy');

            if (jEl.not(':visible')) {
                jEl.pauseTooltip = true;

                if (tooltip) {
                    tooltip.hide();
                }
            }

            $timeout(function() {
                jEl.pauseTooltip = false;
                if (tooltip) {
                    tooltip.hide();
                }
            }, 400);
        }

        return {
            restrict: 'A',
            link: function(scope, el, attributes) {

                var offset = [0, 10];

                if (attributes.titleTop) {
                    offset = [0, -10];
                }

                setTimeout(function() {
                    var jEl = $(el);
                    if (jEl.length) {

                        jEl.tooltipsy({
                            offset: offset,
                            className: attributes.titleTop ? 'tooltipsy top' : 'tooltipsy',
                            show: function (e, $el) {
                                if (!jEl.pauseTooltip) {
                                    $el.show();
                                }
                            }
                        })
                        .on('click', hideToolTip.bind(this, jEl));
                    }
                }, 0);
            }
        };
    }

    angular.module('muzli').directive('title', ['$timeout', muzliTooltip]);

    angular.module('muzli').directive('autofocus', ['$timeout', function($timeout) {
        return {
            restrict: 'A',
            link: function($scope, $element, $attributes) {
                $timeout(function() {
                    $element[0].focus();
                }, $attributes.autofocus || 0);
            }
        }
    }]);

    angular.module('muzli').directive('clickCopy', [function() {
        return {
            restrict: 'A',
            link: function($scope, $element, $attributes) {

                $element.click(function() {

                    document.oncopy = function(event) {
                        event.clipboardData.setData('text/plain', $attributes.clickCopy);
                        event.preventDefault();

                        $scope.copySuccess = true;

                        setTimeout(function() {
                            $scope.copySuccess = false;
                        }, 1000);
                    };

                    document.execCommand("Copy", false, null);
                })
            }
        }
    }]);
    
    angular.module('muzli').directive('clickCopyColor', [function() {
        return {
            restrict: 'A',
            link: function($scope, $element, $attributes) {

                $element.click(function() {

                    document.oncopy = function(event) {

                        event.clipboardData.setData('text/plain', $attributes.clickCopyColor);
                        event.preventDefault();

                        setTimeout(function() {

                            var jEl = $($element);

                            if (jEl.length) {

                                var copySuccessTooltip = jEl.tooltipsy({
                                    content: 'Copied',
                                    offset: [0, -10],
                                    className: 'tooltipsy top',
                                    show: function (e, $el) {
                                        $el.show();
                                    }
                                })
                                .trigger('mouseenter')

                                setTimeout(function() {

                                    jEl.tooltipsy({
                                        content: $attributes.clickCopyColor,
                                        offset: [0, -10],
                                        className: 'tooltipsy top',
                                        show: function (e, $el) {
                                            $el.show();
                                        }
                                    })

                                }, 1000);
                            }

                        }, 0);
                    };

                    document.execCommand("Copy", false, null);
                })
            }
        }
    }]);

    angular.module('muzli').directive('scrollEvents', ['$timeout', function($timeout) {
        return {
            restrict: 'A',
            link: function($scope, $element, $attributes) {

                var preloadOffsetRatio = 2;
                var customScrollbar = false;
                var scrollTimeout;
                var wheelTimeout;
                var wrapper;
                var container;
                var customScrollbarContainer;

                var _initScrollEvents = function() {

                    $(wrapper).scroll(function(event) {

                        if (!!scrollTimeout) {
                            clearTimeout(scrollTimeout);
                        }

                        scrollTimeout = setTimeout(function() {
                            $scope.checkScrollPosition();
                        }, 200);
                    });

                    $(wrapper).bind('mousewheel', function(event) {

                        if (!!scrollTimeout) {
                            clearTimeout(scrollTimeout);
                        }

                        if (event.originalEvent.wheelDelta / 120 < 0) {

                            if (!wheelTimeout) {

                                $scope.checkScrollPosition();

                                wheelTimeout = setTimeout(function() {
                                    clearTimeout(wheelTimeout);
                                    wheelTimeout = false;
                                }, 500);
                            }
                        }
                    });
                };

                $scope.checkScrollPosition = function() {

                    var scrolledOffset = (wrapper === window) ? window.scrollY : container.scrollTop
                    var offsetBottom = container.scrollHeight - container.clientHeight;

                    //Compensate for scrolled distance
                    offsetBottom -= scrolledOffset;

                    //Compensate for custom scrollbar working with absolute positioning
                    offsetBottom += customScrollbarContainer ? customScrollbarContainer.position().top : 0;

                    if (offsetBottom < preloadOffsetRatio * container.clientHeight) {
                        $scope.$emit('scrollAtBottom');
                    }
                };

                $scope.initScrollEvents = function() {
                    _initScrollEvents();
                };


                /*============================
                =            Init            =
                ============================*/

                //Manual init
                if ($attributes.scrollEventsInit === 'manual') {

                    $scope.$on('initScrollEvents', function () {

                        container = $element.parent().get(0);
                        wrapper = container;

                        _initScrollEvents();
                    });

                    $scope.$on('checkScrollPosition', function () {

                        if (!container) {
                            return;
                        }

                        $scope.checkScrollPosition();
                    });

                    return;
                };

                //Window level scroll
                if ($attributes.scrollEventsInit === 'window') {

                    wrapper = window;
                    container = $('body').get(0);

                    _initScrollEvents();

                    return;
                };

                //Auto init
                container = $element.parent().get(0);

                if ($(container).is('body')) {
                    wrapper = window;
                } else {
                    wrapper = container;
                }

                _initScrollEvents();

            }
        }
    }]);

    angular.module('muzli').directive('bindHtmlCompile', ['$compile', function ($compile) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                scope.$watch(function () {
                    return scope.$eval(attrs.bindHtmlCompile);
                }, function (value) {
                    element.html(value);
                    $compile(element.contents())(scope);
                });
            }
        };
    }]);

    angular.module('muzli').filter('filterHome', [function() {
        return function(items) {
            return items.filter(function(item) {
                return !item.isHome;
            })
        }
    }]);

    angular.module('muzli').filter('thousandSuffix', [function () {
        return function (input, decimals) {

            var suffixes = ['k', 'M', 'G', 'T', 'P', 'E'];
            var rounded;
            var exp;

            if (window.isNaN(input)) {
                return null;
            }

            if (input < 1000) {
                return input;
            }

            exp = Math.floor(Math.log(input) / Math.log(1000));

            return (input / Math.pow(1000, exp)).toFixed(decimals) + suffixes[exp - 1];
        };
    }]);

    angular.module('muzli').directive('selectOnClick', [function () {
        return {
            restrict: 'A',
            link: function (scope, element) {
                element.bind('click', function () {
                    this.select();
                });
            }
        };
    }]);

    angular.module('muzli').directive('preventClick', function () {
        return {
            restrict: 'A',
            link: function (scope, element) {
                element.bind('click', function (e) {
                    e.preventDefault();
                });
            }
        };
    });

    angular.module('muzli').directive('preventClick', function () {
        return {
            restrict: 'A',
            link: function (scope, element) {
                element.bind('click', function (e) {
                    e.preventDefault();
                });
            }
        };
    });

    angular.module('muzli').directive('isEuUser', ['storage', function(storage) {
        return {
            restrict: 'A',
            scope: false,
            link: function($scope, $element, $attributes) {
                storage.get('isGDPRuser')
                .then(function(response) {
                    if (response.isGDPRuser) {
                        $scope.isGDPR = true;
                    }
                })
            }
        }
    }]);

    angular.module('muzli').filter('timeAgo', [function() {

        return function(date) {

            if (!date) {
                return 'No date specified';
            };

            var templates = {
                prefix: "",
                suffix: " ago",
                seconds: "less than a minute",
                minute: "a minute",
                minutes: "%d minutes",
                hour: "an hour",
                hours: "%d hours",
                day: "a day",
                days: "%d days",
                month: "about a month",
                months: "%d months",
                year: "about a year",
                years: "%d years"
            };

            var template = function(t, n) {
                return templates[t] && templates[t].replace(/%d/i, Math.abs(Math.round(n)));
            };

            var timer = function(time) {

                var now = new Date();
                var seconds = ((now.getTime() - time) * .001) >> 0;
                var minutes = seconds / 60;
                var hours = minutes / 60;
                var days = hours / 24;
                var years = days / 365;

                return templates.prefix + (
                        seconds < 45 && template('seconds', seconds) ||
                        seconds < 90 && template('minute', 1) ||
                        minutes < 45 && template('minutes', minutes) ||
                        minutes < 90 && template('hour', 1) ||
                        hours < 24 && template('hours', hours) ||
                        hours < 42 && template('day', 1) ||
                        days < 30 && template('days', days) ||
                        days < 45 && template('month', 1) ||
                        days < 365 && template('months', days / 30) ||
                        years < 1.5 && template('year', 1) ||
                        template('years', years)
                        ) + templates.suffix;
            };

            return timer(new Date(date))

        };
    }]);

    angular.module('muzli').directive('apiVersion', ['server', function (server) {
      return {
          restrict: 'E',
          templateUrl: 'modules/muzli/version-info.html',
          link: function (scope, element) {

                if (!window.setEnv) {
                    element.remove();
                    return;
                }

                scope.environments = Object.keys(window.environments)

                scope.currentEnvironment = window.MUZLI_ENV;

                scope.setEnvironment = function(environment) {
                    window.setEnv(environment)
                    scope.currentEnvironment = window.MUZLI_ENV;
                } 
          }
      };
    }]);    

    angular.module('muzli').directive('brightness', ['server', function (server) {
      return {
          restrict: 'A',
          link: function ($scope, $element, $attributes) {

                function brightnessByColor (color) {
      
                    var m = color.substr(1).match(color.length == 7 ? /(\S{2})/g : /(\S{1})/g);
                    if (m) var r = parseInt(m[0], 16), g = parseInt(m[1], 16), b = parseInt(m[2], 16);
                  
                    if (typeof r != "undefined") return ((r*299)+(g*587)+(b*114))/1000;
                }

                if (brightnessByColor($attributes.brightness) < 60) {
                    $element.addClass('dark')
                } 
          }
      };
    }]);

    angular.module('muzli').directive('videoLoader', [function () {
      return {
          restrict: 'A',
          link: function ($scope, $element, $attributes) {

                var parent = $element.parents('li');

                parent.addClass('loading-video')

                $element.one('canplay', function(event) {
                    parent.removeClass('loading-video')
                })

          }
      };
    }]);

})();
