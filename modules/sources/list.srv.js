(function () {

  service.$inject = ['$q', '$http', '$rootScope', 'server', 'sources_list', 'storage', 'userService', 'bundles_list'];
  function service ($q, $http, $rootScope, server, sources, storage, userService, bundles_list) {

    var latestFeedFetch = fetchLatestFromServer();

    var muzli = {
      name: 'muzli',
      title: 'Our Picks',
      description: 'Hand picked, today\'s best design content.',
      channel: 'design',
      url: 'muz.li',
      weight: 2
    };

    function fetchLatestFromServer () {
      return $http({
        method: 'GET',
        url: server() + '/latest'
      }).then(function (res) {
        return res.data.latest;
      });
    }

    function fetchSources () {
      return userService.getData().then(function (res) {
        return res.sources || [];
      }).catch(function (err) {
        if (err && err.status === 401) {
          return [];
        }
        return $q.reject(err);
      });
    }

    function fetchMuzli () {
      return storage.get('muzli').then(function (res) {
        return res.muzli;
      });
    }

    function findByName (sources, name) {
      return sources.filter(function (item) {
        return item.name === name;
      })[0];
    }

    // since server may not always return the most recent posts newest latest needs
    // to be saved locally and compared to result
    function chooseLatest (local, remote) {

      if (local && !remote) {
        return local;
      }

      if (!local && remote) {
        return remote;
      }

      return new Date(local) < new Date(remote) ? remote : local;
    }

    function isRead (lastRead, latest) {

      if (!latest) {
        return true
      }

      if (latest && !lastRead) {
        return false;
      }

      return !(new Date(lastRead) < new Date(latest));
    }

    function fetchMuzliLatest () {

      return $q.all([fetchMuzli(), latestFeedFetch]).then(function (res) {

        var localMuzli = res[0] || { lastRead: new Date().getTime() };
        var latest = res[1][muzli.name];

        var _latestMuzli = chooseLatest(localMuzli.latest, latest);

        return {
          latest: _latestMuzli.latest,
          lastRead: localMuzli.lastRead,
          read: isRead(localMuzli.lastRead, latest)
        };
      });
    }

    function fetchLatest () {

      return $q.all([fetchSources(), latestFeedFetch]).then(function (res) {

        var localSources = res[0];
        var latest = res[1];

        return localSources.map(function (source) {

          var savedSource = findByName(localSources, source.name) || {
            enabled: bundles_list.default.indexOf(source.name) !== -1,
            lastRead: new Date().getTime()
          };

          var _latestItemDate = chooseLatest(savedSource.latest, latest[source.name]);

          return {
            name: source.name,
            latest: _latestItemDate,
            enabled: savedSource.enabled,
            lastRead: savedSource.lastRead,
            read: isRead(savedSource.lastRead, _latestItemDate)
          }
        });
      });
    }

    return {
      findByName: findByName.bind(this, sources.concat([muzli])),
      fetchMuzli: function () {
        return fetchMuzliLatest().then(function (localMuzli) {
          return {
            name: muzli.name,
            title: muzli.title,
            read: localMuzli && localMuzli.read,
            latest: localMuzli && localMuzli.latest,
            lastRead: localMuzli && localMuzli.lastRead
          };
        }).catch(function () {
          return {
            name: muzli.name,
            title: muzli.title
          };
        });
      },
      fetch: function (skipCache) {

        if (skipCache) {
          latestFeedFetch = fetchLatestFromServer();
        }

        return fetchLatest().then(function (localSources) {

          var mappedSources = sources.map(function (source, index) {

            var savedSource = findByName(localSources, source.name) || {
              enabled: bundles_list.default.indexOf(source.name) !== -1
            };

            var position = localSources.indexOf(savedSource);

            return {
              name: source.name,
              tags: source.tags,
              icon: source.icon,
              nsfw: source.nsfw,
              base64: source.base64,
              title: source.title,
              url: source.url ? encodeURIComponent("http://" + source.url) : source.url,
              description: source.description,
              position: position > -1 ? position : index,
              enabled: savedSource.enabled,
              read: savedSource.read,
              latest: savedSource.latest,
              lastRead: savedSource.lastRead,
              featured: source.featured,
              new: source.new,
            }
          });

          mappedSources.sort(function (source, _source) {
            return source.position - _source.position;
          });

          mappedSources.forEach(function (source) {
            delete source.position;
          });

          return mappedSources;
        });
      },
      getWeightedSources: function () {

        return fetchLatest().then(function (localSources) {

          var enabledSources = localSources.filter(function (source) {
            return source.enabled;
          })

          //Set default sources if no salection is saved
          if (!enabledSources || !enabledSources.length) {
            enabledSources = sources
            .filter(function (source) {
              return bundles_list.default.indexOf(source.name) !== -1
            })
          }

          enabledSources = enabledSources.map(function (source) {
            return {
              name: source.name,
              weight: source.weight || 1
            }
          })
          .concat([muzli])

          return enabledSources;

        });
      },

      sync: function (sources, syncAsTemp) {

        sources = sources.map(function (source) {
          return {
            name: source.name,
            enabled: source.enabled,
            latest: source.latest,
            lastRead: source.lastRead || source.latest || new Date(),
          };

        });

        return userService.setData(syncAsTemp ? {
          _sources: sources
        } : {
          sources: sources
        });

      },

      syncMuzli: function (muzli) {
        var res = {
          name: muzli.name,
          latest: muzli.latest,
          lastRead: muzli.lastRead || muzli.latest || new Date(),
        };

        storage.set({
          muzli: res
        });
      }
    }
  }

  angular.module('sources')
    .factory('sources', service);

})();
