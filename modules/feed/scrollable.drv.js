(function() {

    var $window = $(window);

    scrollableFeed.$inject = ['$rootScope', '$timeout', 'trackService', 'localPageSize', 'serverPageSize', 'userService', 'feedFetcher', 'sources'];

    function scrollableFeed($rootScope, $timeout, trackService, localPageSize, serverPageSize, userService, feedFetcher, sources) {

        return {
            restrict: 'E',
            scope: {
                items: '=',
                sponsored: '=',
            },
            transclude: {
                'no-data': '?scrollableFeedNoData'
            },
            templateUrl: function(elem, attrs) {

                if ($rootScope.currentSource === 'favorites') {
                    return 'modules/feed/feed.drv.favorites.html'
                }

                if ($rootScope.currentSource.name === 'colors') {
                    return 'modules/feed/feed.drv.colors.html'
                };

                return 'modules/feed/feed.drv.html'
            },
            link: function($scope, el, attrs) {

                var feedItems;
                var current;
                var feed = [];
                var checkDeleteLength = angular.isDefined(attrs.checkDeleteLength);
                var source = $rootScope.currentSource.name || $rootScope.currentSource;
                var loadComplete = false;
                var loadingFromServer = false;
                var sourceCounter = {};

                $scope.loading = false;
                $scope.numberOfServerPageLoads = 0;
                $scope.infiniteScrollDistance = window.muzli.paging.scrollDistance;
                $scope.showFavorite = angular.isDefined(attrs.showFavorite);
                $scope.showVirality = angular.isDefined(attrs.showVirality);
                $scope.enablePalettes = $rootScope.enablePalettes;

                function init() {

                    // If feed array already exist, reset it
                    if (feed.length) {
                        feed.length = 0;
                    }

                    //Initialy load feedItems from passed directive parameter
                    feedItems = $scope.items;
                    current = 0;

                    $scope.feed = feed;

                    userService.getData().then(function(user) {
                        $scope.user = user;
                    })
                }

                function insertItems() {

                    $scope.loadingItems = true;

                    //Slice feedItems array according to local pagination
                    var feedPage = feedItems.slice(current, Math.min(current + localPageSize, feedItems.length));

                    //Add links that redirect to Muzli share splash page
                    addShareLinks(feedPage, current);

                    current += localPageSize;

                    //Ad page to feed
                    feed.push.apply(feed, feedPage);

                    setTimeout(function() {
                        $scope.loadingItems = false;
                    }, 0)
                }

                function loadMore() {

                    var past = current > feedItems.length - 1;

                    if (past && source && !loadingFromServer && !loadComplete) {

                        loadingFromServer = true;

                        feedFetcher.fetchFromServer(source, serverPageSize, current).then(function(res) {

                            if (!res.length) {
                                loadComplete = true;
                                return;
                            }

                            Array.prototype.push.apply(feedItems, res.filter(function(item) {
                                return !feedItems.filter(function(_item) {
                                    return _item.id === item.id;
                                }).length
                            }));

                            insertItems();
                            $scope.numberOfServerPageLoads++
                            loadingFromServer = false;
                        });
                    }

                    if (past) {
                        return;
                    }

                    insertItems();
                }

                function addShareLinks(feedPage, current) {

                    if (source === 'favorites') {
                        return;
                    }

                    if (!window.MUZLI_SHARE_SERVER) {
                        return;
                    }

                    if (!JSON.parse(localStorage.enableSharebleLinks || 'false')) {
                        return;
                    }

                    if (window.innerWidth <= 1440) {
                        return;
                    }

                    feedPage.forEach(function(item, index) {

                        var source = (item.source ? item.source.name : item.source) || 'muzli';
                        var listPosition = sourceCounter[source] - 1;
                        var link = encodeURIComponent(item.link);
                        var shortUrl = btoa(item.id.slice(0,9))
                        var linkOut = window.MUZLI_SHARE_SERVER + shortUrl

                        if (!item.isFrameAllowed) {
                            return;
                        }

                        //Increment source counter
                        if (!sourceCounter[source]) {
                            sourceCounter[source] = 0;
                        }

                        sourceCounter[source] ++;

                        linkOut += '?referrer=muzli&source=' + source + '&skip=' + listPosition;
                        item.link_out = linkOut;

                    })
                }

                $scope.loadMore = loadMore;
                $scope.playerVars = feedFetcher.constants.playerVars;
                $scope.postClick = feedFetcher.event.postClick;
                $scope.openSharer = feedFetcher.event.openSharer;
                $scope.sendSlack = feedFetcher.event.sendSlack;
                $scope.sourceClick = feedFetcher.event.sourceClick;
                $scope.promotionClick = feedFetcher.event.promotionClick;
                $scope.toggleFavorite = feedFetcher.event.toggleFavorite;
                $scope.markNSFW = feedFetcher.event.markNSFW;
                $scope.unmarkNSFW = feedFetcher.event.unmarkNSFW;

                if ($rootScope.feedVisibleClass) {
                    $scope.showAdPixel = true;
                }

                $scope.markHidden = function(item) {
                    feedFetcher.event.markHidden(item).then(function(response) {
                        var index = $scope.feed.indexOf(item);

                        if (index > -1) {
                            $scope.feed.splice(index, 1);
                        }
                    });
                }

                $scope.downloadSVG = function(item, event) {

                    if (event) {
                        event.stopPropagation();
                        event.preventDefault();     
                    }

                    function pad(num) {
                        var s = num+"";
                        while (s.length < 2) s = "0" + s;
                        return s;
                    }

                    trackService.track({
                        category: 'Palettes',
                        action: 'Download'
                    });

                    var useExternalSearch = $rootScope.user.searchVersion === 'web';
                    var downloadUrl;

                    if (useExternalSearch) {
                        downloadUrl = window.MUZLI_SEARCH_URL + '/palette/' + item.palette.map(function(color) {
                            return color.slice(1)
                        }).join('/') + '?utm_source=extension&utm_medium=muzli'
                    } else {                    
                        downloadUrl = window.MUZLI_COLORS_SERVER + '/download/' + btoa(item.id.slice(0,9)) + '?time='
                        var now = new Date();

                        downloadUrl += now.getFullYear()
                        downloadUrl += pad(now.getMonth())
                        downloadUrl += pad(now.getDate())
                        downloadUrl += pad(now.getHours())
                        downloadUrl += pad(now.getMinutes())
                    }

                    window.open(downloadUrl);

                }

                $scope.videoClick = function(post, event) {
                    event.stopPropagation();
                    event.preventDefault();

                    feedFetcher.event.videoClick(post);
                };

                $scope.openDNlink = function(e, link) {

                    e.stopPropagation();
                    e.preventDefault();

                    window.open(link);
                };

                $scope.removeFavorite = function(event, index, item) {
                    event.preventDefault();
                    event.stopPropagation();

                    userService.setFavorite(item, false);

                    feed.splice(index, 1);
                };

                $scope.addFeed = function(item, $event) {
                    $event.preventDefault();
                    $event.stopPropagation();

                    trackService.track({
                        category: 'SignIn',
                        action: 'Feed promo button click'
                    });

                    $rootScope.sources.filter(function(_item) {
                        return item.source.name === _item.name;
                    })[0]._enabled = true;

                    sources.sync($rootScope.sources.map(function(item) {
                        return {
                            name: item.name,
                            enabled: item._enabled,
                            lastRead: item.lastRead
                        };
                    }), true).then(function() {
                        $rootScope.vm.showSignInDialog = 'sourceFeedPromo';
                    });
                };

                if (checkDeleteLength) {

                    $scope.$watch('feed.length', function(value, oldValue) {

                        if (value === 0 && oldValue) {
                            
                            $scope.blockEmpty = true;

                            $timeout(function() {
                                $scope.blockEmpty = false;
                            }, 600);
                        }
                    });
                }

                var triggerImageUpdate = debounce(function () {

                    var viewPortThreshold = window.muzli.lazyLoading.viewPortThreshold;
                    var windowScroll = $window.scrollTop();

                    //Do not trigger offset hiding until user scrolls down to 10k px
                    if (windowScroll < 10000 && !$scope.hideOutboundItems) {
                        return;
                    } else {
                        $scope.hideOutboundItems = true;
                    }

                    $scope.feed.forEach(function(item) {
                        if (windowScroll > item.offsetTop + viewPortThreshold) {
                            item.isOutside = true;
                        } else {
                            item.isOutside = false
                        }
                    })

                }, window.muzli.lazyLoading.imageDebounce);

                $scope.$watch('items', function(items) {   
                    if (items && items.length) {

                        init();

                        $scope.isRendering = true;

                        $timeout(function() {
                            loadMore();
                            $scope.isRendering = false;
                        });

                    } else if (items) {
                        init();
                    }
                });

                $scope.$on('$stateChangeStart', function() {
                    $scope.loadMore = null;
                });

                $scope.$on('user-scrolled', function(aaa) {
                    $scope.showAdPixel = true;
                });
            }
        };
    }

    angular.module('feed')
        .directive('scrollableFeed', scrollableFeed)
        .directive('muzliPlaceholder', ['$rootScope', function($rootScope) {
            return {
                restrict: 'A',
                scope: {
                    placeholder: '=ngPlaceholder'
                },
                link: function(scope, elem) {
                    $rootScope.$watch('activeSearch', function(value) {
                        if (!value) {
                            return;
                        }
                        elem[0].placeholder = value === 'muzli' ? 'Inspiration search' : 'Search Google or type URL';
                    });
                }
            }
        }]);

})();
