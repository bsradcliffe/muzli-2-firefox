/* global YT */
angular.module('feed')
  .directive('muzliVideo', ['$http', '$rootScope', '$timeout', function ($http, $rootScope, $timeout) {

    return {
      restrict: 'EA',
      templateUrl: 'modules/feed/video.drv.html',
      link: function ($scope, element) {

        //Set item as home if it is visible
        if (element.parents('li').is(':visible') && !!element.parents('.home-overlay').length) {
            $scope.item.isHome = true;
        }

        $scope.item.playing = false;

        var $window = $(window)
        var container = element.parents('li');
        var videoContainer = $('.video-container');

        //Use global debounce function to limit checks once per 100ms
        $scope.trackScrollDistance = debounce(function(e) {

            var videoOutOfBounds = (container.offset().top - $window.scrollTop() - 65) < 0

            if (videoOutOfBounds) {
              $scope.popVideo();
            }
        }, 100)

        $scope.getTileDimensions = function() {
          return {
            top: container.offset().top,
            left: container.offset().left - 65,
            width: container.width(),
            height: container.height(),
          }
        }

        $scope.popVideo = function() {

          videoContainer.addClass('pop');
          videoContainer.css({
            top: '',
            left: '',
            width: '',
            height: '',
          })

          $scope.isPopup = true;
          $window.off("wheel scroll", $scope.trackScrollDistance);
          videoContainer.removeClass('no-padding');
        }

        $scope.imageClicked = function ($event) {

          $event.preventDefault();
          $event.stopPropagation()

          //Fix container height and double size
          container.css({
            height: container.height()
          })
          container.addClass('double'); //Use this to workaround angular event loop and add class faster
          
          //Marking item as playing adds class to double size the item.
          $scope.item.playing = true;

          //Reset item
          $scope.isPopup = false;
          videoContainer.removeClass('pop');
          videoContainer.removeClass('no-padding');

          if (container.parents('#sticky').length) {
            videoContainer.addClass('no-padding');
          }

          //Listen for scroll events
          $window.on("wheel scroll", $scope.trackScrollDistance);

          //If the same element was clicked change nothing
          if ($rootScope.currentPlayingItem === $scope) {
            videoContainer.css($scope.getTileDimensions());
            return;
          }

          //Init video iframe
          var videoElement; 

          if (!$scope.item.htmlVideo) {
            videoElement = $('<iframe frameborder="0" allowfullscreen webkitallowfullscreen mozallowfullscreen>');
          } else {
            videoElement = $('<video id="video" playsinline autoplay controls="controls">');
          }
          
          videoElement.attr('src', $scope.item.video);

          //Stop previouosly playing item
          if ($rootScope.currentPlayingItem && $rootScope.currentPlayingItem !== $scope) {

            $rootScope.currentPlayingItem.item.playing = false;
            $rootScope.currentPlayingItem.isPopup = false;

            $window.off("wheel scroll", $rootScope.currentPlayingItem.trackScrollDistance);
          }

          //Set current item as currently playiong
          $rootScope.currentPlayingItem = $scope;

          //Form the container for video and append container to it.
          $timeout(function() {

            videoContainer.css($scope.getTileDimensions());

            videoContainer.find('.iframe-wrapper').html(videoElement);
                        
          });

          //If window is resized, pop video automatically
          function popOnResize() {

              videoContainer.addClass('pop');
              videoContainer.css({
                top: '',
                left: '',
                width: '',
                height: '',
              })

              $scope.isPopup = true;
              
              videoContainer.removeClass('no-padding');
              $window.off("wheel scroll", $scope.trackScrollDistance);
              $window.off("resize", popOnResize);
          }

          $window.on('resize', popOnResize);

          var schrollWatcher = $rootScope.$on('user-scrolled', function() {
            popOnResize()
            schrollWatcher()
          });

          //Destroy container if video is not popped and user navigated away
          $scope.$on('$destroy', function() {

            if ($rootScope.currentPlayingItem && !$rootScope.currentPlayingItem.isPopup) {
              $rootScope.closeVideoPopup();
            }

            $window.off("wheel scroll", $scope.trackScrollDistance);
            videoContainer.removeClass('no-padding');

          });

          //Log video click
          $http.put(window.MUZLI_APP + '/click', {
            id: $scope.item.id,
            link: $scope.item.link,
            source: $scope.item.source.name ? $scope.item.source.name : $scope.item.source,
            referer: 'Extension video'
          })          
        };

        $scope.$on("$destroy", function() {
          if ($scope.item.playing && !$scope.isPopup) {
            $scope.popVideo()
          }
        })

        if ($scope.item.vimeo && !$scope.item.thumbnail) {

          if (!$scope.item.image) {
            $http.get('https://vimeo.com/api/v2/video/' + $scope.item.videoId + '.json', {
              skipAuth: true
            }).then(function (res) {
              $scope.item.thumbnail = res.data[0].thumbnail_large;
            }).catch(function(){
              element.parents('.postPhoto').addClass('image-error');
            })
          } else {
            $scope.item.thumbnail = $scope.item.image;
          }
        }

        if ($scope.item.htmlVideo) {
          $scope.item.thumbnail = $scope.item.image;
        }

      }
    }
  }]);
