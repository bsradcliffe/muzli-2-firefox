(function () {

  var $window = $(window);
  var scrollStart = false;
  var emptyImage = window.muzli.emptyImage;

  function setImage ($self, src) {
    if ($self.is("img")) {
      $self.attr("src", src);
    } else {
      $self.css("background-image", "url('" + src + "')");
    }
  }

  function drawImageProp(ctx, img, x, y, w, h, offsetX, offsetY) {

    if (arguments.length === 2) {
        x = y = 0;
        w = ctx.canvas.width;
        h = ctx.canvas.height;
    }

    // default offset is center
    offsetX = typeof offsetX === "number" ? offsetX : 0.5;
    offsetY = typeof offsetY === "number" ? offsetY : 0.5;

    // keep bounds [0.0, 1.0]
    if (offsetX < 0) offsetX = 0;
    if (offsetY < 0) offsetY = 0;
    if (offsetX > 1) offsetX = 1;
    if (offsetY > 1) offsetY = 1;

    var iw = img.naturalWidth,
        ih = img.naturalHeight,
        r = Math.min(w / iw, h / ih),
        nw = iw * r,   // new prop. width
        nh = ih * r,   // new prop. height
        cx, cy, cw, ch, ar = 1;

    // decide which gap to fill
    if (nw < w) ar = w / nw;
    if (Math.abs(ar - 1) < 1e-14 && nh < h) ar = h / nh;  // updated
    nw *= ar;
    nh *= ar;

    // calc source rectangle
    cw = iw / (nw / w);
    ch = ih / (nh / h);

    cx = (iw - cw) * offsetX;
    cy = (ih - ch) * offsetY;

    // make sure source rectangle is valid
    if (cx < 0) cx = 0;
    if (cy < 0) cy = 0;
    if (cw > iw) cw = iw;
    if (ch > ih) ch = ih;

    // fill image in dest. rectangle
    ctx.drawImage(img, cx, cy, cw, ch,  x, y, w, h);
  }

  function loadImage (config) {

    config = config || {};

    var self = this;
    var $self = $(this);
    var useLoadedClass = config.useLoadedClass
    var src = config.image;
    var gif = config.gif;
    var isGif = config.isGif;
    var currentSrc = $self.attr('src');
    var defer = $.Deferred();

    if (!src) {
      src = $self.attr('muzli-lazy');
      gif = $self.attr('muzli-gif');
      isGif = $self.attr('muzli-is-gif');
    }

    if (currentSrc === src) {
      return $.when();
    }

    $('<img src="' + src + '"/>')
      .bind("error", function () {

        var parentTopLi = $self.parents('li');

        $self.parents('.tile').addClass("image-error");
        $self.css({'opacity': '1'});

        if (config.fallbackImageSrc){
          $self.attr('src', config.fallbackImageSrc);
        } else {
          $self.attr('src', emptyImage);
        }

        parentTopLi.css({ 'background-image': 'none'});

        defer.resolve();

      })
      .bind("load", function () {

        setImage($self, this.src);

        if (gif && gif !== src) {

          var parent = $self.parents('.postPhoto');
          var parentTopLi = $self.parents('li');
          var cacheImage = new Image();
          cacheImage.src = gif;

          $(parentTopLi).mouseenter(function () {
            parentTopLi.addClass('loading-gif');
            setImage($self, gif);

            $self.one('load', function () {
              parentTopLi.removeClass('loading-gif');
            });
          });

          $(parentTopLi).mouseleave(function () {
            $self.attr('src', src);
          });
        }

        else if (isGif) {

          if (!self.parentNode) {
            defer.resolve();
            return;
          }

          function drawCoverCanvas() {

            var parentTopLi = $self.parents('li');
            var parent = $self.parents('.tile .postPhoto');
            var canvas = document.createElement('canvas');

            var width = self.parentNode.offsetWidth;
            var height = self.parentNode.offsetHeight;

            canvas.width = width;
            canvas.height = height;

            //Use mapper function to draw cover image on canvas
            drawImageProp(canvas.getContext('2d'), self);

            parent.append(canvas);

            $self.remove();

            $(parentTopLi).mouseenter(function () {

              $self.attr('src', src);
              parent.append($self);

              $self.one('load', function () {
                $(canvas).hide();
              });
            });

            $(parentTopLi).mouseleave(function () {

              $self.attr('src', emptyImage);
              $self.remove();

              $(canvas).show();
            });
          }

          //If item has no dimensions, add a trigger to render canvas, when it gets visible
          if (!self.parentNode.offsetWidth || !self.parentNode.offsetHeight) {

            $self.addClass('wait-for-canvas');

            $self.one('loadCanvas', function() {
              $self.removeClass('wait-for-canvas');
              drawCoverCanvas()
            })

          } else {
            drawCoverCanvas();
          }
        }

        defer.resolve();

      });
    
    return defer.promise();
  }

  window.muzli.pageChange = function () {
    $('.tooltipsy').remove();
    $window.scrollTop(0);
    setTimeout(function () {
      scrollStart = false;
    }, 0);
  }

  lazyImage.$inject = ['$rootScope', '$timeout'];

  // For handling initial loading only the rest is handled by the $(window).on('resize scroll').
  function lazyImage ($rootScope, $timeout) {
    return {
      restrict: 'A',
      link: function (scope, elm) {

        // scope.thumbnail - video post
        // scope.item - regular post
        // !scope.thumbnail && !scope.item - promoted post

        $rootScope.imageQueue = $rootScope.imageQueue || [];
        $rootScope.visibleImageQueue = $rootScope.visibleImageQueue || [];

        var itemElement = elm.parents('li');

        //Set element's offset from top, to track it's position
        if (!$rootScope.feedVisibleClass) {
          $rootScope.$watch('feedVisibleClass', function(value) {
            if (value) {
              $timeout(function() {

                var offset = itemElement.offset();

                if (offset) {
                  scope.item.offsetTop = offset.top;
                }
              });
            }
          });
        } else {

          var offset = itemElement.offset();

          if (offset) {
            scope.item.offsetTop = offset.top;
          }
        }

        //If window resizes, recalculate item position
        $(window).on('resize', function() {

          var offset = itemElement.offset();

          if (offset) {
            scope.item.offsetTop = offset.top;
          }
        });

        if (!scope.thumbnail && !scope.item) {
          
          setTimeout(function () {
            loadImage.call(elm[0], config)
          }, 0)

          return;
        }


        if (scope.item.vimeo && !scope.item.image && !scope.item.thumbnail) {
          return;
        }


        var imageLoader = function() {

          loadImage.call(elm[0], {
            scope: scope,
            image: scope.item.thumbnail ? scope.item.thumbnail : scope.item.image,
            fallbackImageSrc: scope.item.fallbackImageSrc,
            gif: scope.item.thumbnail ? false : scope.item.gif,
            isGif: scope.item.thumbnail ? false : scope.item.isGif,
          }).done(function() {

            //Show element after ir was loaded
            itemElement[0].classList.add('show');

            if ($rootScope.areHomeImagesLoaded) {
              return;
            }
            
            var index = $rootScope.visibleImageQueue.indexOf(imageLoader);

            if (index !== -1) {
              $rootScope.visibleImageQueue.splice(index, 1);
            }

            //Load other images;
            if (!$rootScope.visibleImageQueue.length) {
              
              //Broadcast image load event
              $rootScope.$broadcast('homeImagesLoaded');
              $rootScope.areHomeImagesLoaded = true;

              //Load other images 
              setTimeout(function() {
                $rootScope.imageQueue.forEach(function(imageLoader) {
                  imageLoader()
                })
              }, 250);
            }

          })
        }

        //If initial images are loaded, just skip queing
        if ($rootScope.areHomeImagesLoaded) {
          imageLoader();
          return;
        } 

        var isSticky = !!elm.parents('#sticky').length;

        if (!$rootScope.areHomeImagesLoaded) {

          //Load only visible items first
          if (itemElement.is(':visible') && isSticky) {

            scope.item.isHome = !!elm.parents('.home-overlay').length;

            $rootScope.visibleImageQueue.push(imageLoader);

            imageLoader();

          //Load other images into secondary to load quicker queue
          } else {
            $rootScope.imageQueue.push(imageLoader)
          }

          return;

        }


      }
    };
  }

  angular.module('feed').directive('muzliLazy', lazyImage);

})();
